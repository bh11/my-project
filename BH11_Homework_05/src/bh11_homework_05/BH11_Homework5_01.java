package bh11_homework_05;

import java.util.Scanner;

public class BH11_Homework5_01 {

    static int hourGenerator(int a) {
        Scanner scanner = new Scanner(System.in);
        boolean askingHour = false;
        do {
            if (scanner.hasNextInt()) {
                a = scanner.nextInt();
                if (a >= 0 && a <= 24) {
                    askingHour = true;
                    return a;
                }
            } else {
                scanner.next();
            }
        } while (!askingHour);
        return a;
    }

    static int minuteGenerator(int b) {
        Scanner scanner = new Scanner(System.in);
        boolean askingMinute = false;
        do {
            if (scanner.hasNextInt()) {
                b = scanner.nextInt();
                if (b >= 0 && b <= 59) {
                    askingMinute = true;
                    return b;
                }
            } else {
                scanner.next();
            }
        } while (!askingMinute);
        return b;
    }

    static char typeGenerator(char c) {
        Scanner scanner = new Scanner(System.in);

        boolean askingType = false;
        do {
            if (scanner.hasNext()) {
                c = scanner.nextLine().charAt(0);
                if (c == 'c' || c == 'h' || c == 'x') {
                    askingType = true;
                    return c;
                }
            } else {
                scanner.next();
            }
        } while (!askingType);
        return c;
    }

    static int moneyGenerator(int d) {
        Scanner scanner = new Scanner(System.in);
        boolean askingMoney = false;
        do {
            if (scanner.hasNextInt()) {
                d = scanner.nextInt();
                if (d >= 0) {
                    askingMoney = true;
                    return d;
                }
            } else {
                scanner.next();
            }
        } while (!askingMoney);
        return d;
    }

    public static void main(String[] args) {
        /*
        Egy busztársaság szeretné tudni, a napok mely óráiban hány ellenőrt érdemes terepmunkára küldenie.
        Ehhez összesítik a bírságok adatait. Egy adat a következőkből áll: „óra perc típus összeg”, ahol a típusnál
        h a helyszíni bírság, c pedig a csekk. „9 45 c 6000” jelentése: egy utas 9:45-kor csekket kapott 6 000
        forintról. A helyszíni bírságokat az ellenőrök begyűjtik; a csekkes bírságoknak átlagosan 80%-át tudják
        behajtani. (Vagyis egy 6 000-es csekk a társaság számára csak 4 800-at ér.) Az adatsor végén 0 0 x 0
        szerepel.
        Olvassa be a C programod ezeket az adatokat! Készítsen kimutatást arról, hogy mely napszakban mennyi
        a pótdíjakból a bevétel! Példakimenet:
        16:00-16:59, 14800 Ft
        17:00-17:59, 12000 Ft
         */

        Scanner scanner = new Scanner(System.in);
        int hour = -1;
        int minute = -1;
        char type = ' ';
        int money = -1;
        int startNumber = 0;
        int[] penalty = new int[24];
        boolean exit = false;

        System.out.println("Írd be az órát, percet, a büntetés típusát és összegét!");
        System.out.println("Kilépés: 0 0 x 0");
        do {
            System.out.println("Óra: ");
            hour = hourGenerator(hour);
            System.out.println("Perc: ");
            minute = minuteGenerator(minute);
            System.out.println("Típus: ");
            type = typeGenerator(type);
            System.out.println("Összeg: ");
            money = moneyGenerator(money);
            if (hour == 0 && minute == 0 && type == 'x' && money == 0) {
                exit = true;
            }
            if (type == 'c') {
                money = (int) (money * 0.8);
                penalty[hour] += money;
            } else if (type == 'h') {
                penalty[hour] += money;
            }

        } while (!exit);

        for (int i = 0; i < penalty.length; i++) {
            System.out.printf("%d:00-%d:59, %d Ft\n", i, i, penalty[i]);
        }

        // A szépséghibája a feladatnak, hogy space helyett entereket kell ütni.
        // Ha nem függvénybe teszem, akkor megoldható lett volna, de ki akartam próbálni a metódusokat :D
    }

}
