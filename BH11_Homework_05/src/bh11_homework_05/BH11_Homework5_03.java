package bh11_homework_05;

import java.util.Scanner;

public class BH11_Homework5_03 {

    static int isIntNumber(int a) {
        Scanner scanner = new Scanner(System.in);
        boolean askingNumber = false;
        do {
            if (scanner.hasNextInt()) {
                a = scanner.nextInt();
                askingNumber = true;
                return a;
            } else {
                scanner.next();
            }
        } while (!askingNumber);
        return a;
    }

    public static void main(String[] args) {

        /*
        Olvassunk be egész számokat egy 20 elemű tömbbe, majd kérjünk be egy egész számot. Keressük meg a
        tömbben az első ilyen egész számot, majd írjuk ki a tömbindexét. Ha a tömbben nincs ilyen szám, írjuk
        ki, hogy a beolvasott szám nincs a tömbben.
         */
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[20];
        int counter = 0;
        int number = 0;

        System.out.println("Írj be 20db egész számot!");
        do {
            number = isIntNumber(number);
            array[counter]=number;
            counter++;
        } while (counter < 5);

        System.out.println("Ajd meg egy egész számot!");
        int number2 = 0;
        number2 = isIntNumber(number2);

        int index = -1;

        for (int i = 0; i < array.length; i++) {
            if (array[i] == number2) {
                index = i;
            }
        }
        System.out.println(index);
        if (index < 0) {
            System.out.println("A szám nincs a tömbben.");
        } else {
            System.out.println("A szám indexe: " + index);
        }
    }

}
