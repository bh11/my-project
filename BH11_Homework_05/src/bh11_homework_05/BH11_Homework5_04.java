package bh11_homework_05;

import java.util.Scanner;

public class BH11_Homework5_04 {

    static int isBetween09(int a) {
        Scanner scanner = new Scanner(System.in);
        boolean askingNumber = false;
        do {
            if (scanner.hasNextInt()) {
                a = scanner.nextInt();
                if (a >= 0 && a <= 9) {
                    askingNumber = true;
                    return a;
                }
            } else {
                scanner.next();
            }
        } while (!askingNumber);
        return a;
    }

    public static void main(String[] args) {
        /*
        Állítsunk elő egy 50 elemű tömböt véletlen egész számokból (0-tól 9-ig terjedő számok legyenek).
        - Írjuk ki a kigenerált tömböt a képernyőre.
        - Számítsuk ki az elemek összegét és számtani középértékét.
        - Olvassunk be egy 0 és 9 közötti egész számot, majd határozzuk meg, hogy a tömbben ez a szám
        hányszor fordul elő.
         */
        Scanner scanner = new Scanner(System.in);

        int[] array = new int[50];

        int sum = 0;

        for (int i = 0; i < array.length; i++) {

            array[i] = (int) (Math.random() * 9);
            sum += array[i];
        }

        System.out.println("Ajd meg egy számot 0 és 9 között!");
        int number2 = 0;
        number2 =isBetween09(number2);
        
        int counter = 0;

        System.out.print("A tömb elemei: ");
        for (int j = 0; j < array.length; j++) {
            System.out.print(" " + array[j]);
            if (array[j] == number2) {
                counter++;
            }
        }
        System.out.println();
        System.out.println("A számok összege: " + sum);
        System.out.println("A számok átlaga: " + (double) sum / array.length);
        System.out.printf("A megadott szám (%d) %d alkalommal szerepel\n", number2, counter);

    }

}
