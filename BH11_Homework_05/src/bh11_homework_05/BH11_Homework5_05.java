package bh11_homework_05;

import java.util.Scanner;

public class BH11_Homework5_05 {

    static int howManyChar(String sentence, char a) {
        int counter = 0;
        for (int i = 0; i < sentence.length(); i++) {
            if (sentence.charAt(i) == a) {
                counter++;
            }
            if (i == sentence.length() - 1) {
                return counter;
            }
        }
        return 0;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String sentence = scanner.nextLine();
        char character = scanner.next().charAt(0);

        System.out.println(howManyChar(sentence, character));
    }

}
