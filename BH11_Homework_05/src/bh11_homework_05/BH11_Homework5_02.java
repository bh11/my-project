package bh11_homework_05;

import java.util.Scanner;

public class BH11_Homework5_02 {

    static int isIntNumber(int a) {
        Scanner scanner = new Scanner(System.in);
        boolean askingNumber = false;
        do {
            if (scanner.hasNextInt()) {
                a = scanner.nextInt();
                askingNumber = true;
                return a;
            } else {
                scanner.next();
            }
        } while (!askingNumber);
        return a;
    }

    public static void main(String[] args) {

        /*
        Olvassunk be egész számokat 0 végjelig egy maximum 100 elemű tömbbe (a tömböt 100 eleműre
        deklaráljuk, de csak az elejéből használjunk annyi elemet, amennyit a felhasználó a nulla végjelig beír).
        - Írjuk ki a számokat a beolvasás sorrendjében.
        - Írjuk ki az elemek közül a legkisebbet és a legnagyobbat, tömbindexükkel együtt.
        - Írjuk ki az elemeket fordított sorrendben.
         */
        Scanner scanner = new Scanner(System.in);

        int[] array = new int[100];
        int number = -1;
        int counter = 0;
        int min = Integer.MAX_VALUE;
        int minIndex = 0;
        int max = Integer.MIN_VALUE;
        int maxIndex = 0;
        boolean exit = false;
        
        System.out.println("Írj be maximum 100db egész számot!");
        do {
            number = isIntNumber(number);
            if (number == 0) {
                exit = true;
            } else {
                array[counter] = number;
                if (max < array[counter]) {
                    max = number;
                    maxIndex = counter;
                }
                if (min > array[counter]) {
                    min = number;
                    minIndex = counter;
                }
                counter++;
            }
        } while (!exit);

        System.out.print("A tömb elemei: ");
        int[] temp = new int[counter];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = array[i];
            System.out.print(" " + temp[i]);
        }
        System.out.println();
        System.out.printf("Legkisebb elem: %d \nIndexe: %d \n", min, minIndex);
        System.out.printf("Legnagyobb elem: %d \nIndexe: %d \n", max, maxIndex);

        System.out.print("A tömb elemei fordított sorrendben: ");
        for (int i = temp.length - 1; i >= 0; i--) {
            System.out.print(" " + temp[i]);
        }
        System.out.println();
    }
}
