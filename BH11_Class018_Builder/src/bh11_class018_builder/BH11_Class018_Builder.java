
package bh11_class018_builder;


public class BH11_Class018_Builder {


    public static void main(String[] args) {
       
        Car c = new Car.CarBuilder("BMW", "Black")
                .setABS(true)
                .setMaxSpeed(280)
                .setSpoiler(true)
                .build();
        
        Car2 c2 = new Car2("Trabant", "Green")
                .setABS(true)
                .setMaxSpeed(150);
                
    }
    
}
