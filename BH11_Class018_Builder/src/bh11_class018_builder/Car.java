package bh11_class018_builder;

public class Car {

    private String model;
    private String color;
    private boolean ABS;
    private int maxSpeed;
    private boolean spoiler;

    public Car(CarBuilder cb) {
        this.model = cb.model;
        this.color = cb.color;
        this.ABS = cb.ABS;
        this.maxSpeed = cb.maxSpeed;
        this.spoiler = cb.spoiler;
    }

    public static class CarBuilder {

        private String model;
        private String color;
        private boolean ABS;
        private int maxSpeed;
        private boolean spoiler;

        public CarBuilder(String model, String color) {
            this.model = model;
            this.color = color;
        }

        public Car build() {
            return new Car(this);
        }

        public CarBuilder setModel(String model) {
            this.model = model;
            return this;
        }

        public CarBuilder setColor(String color) {
            this.color = color;
            return this;
        }

        public CarBuilder setABS(boolean ABS) {
            this.ABS = ABS;
            return this;
        }

        public CarBuilder setMaxSpeed(int maxSpeed) {
            this.maxSpeed = maxSpeed;
            return this;
        }

        public CarBuilder setSpoiler(boolean spoiler) {
            this.spoiler = spoiler;
            return this;
        }

    }

}
