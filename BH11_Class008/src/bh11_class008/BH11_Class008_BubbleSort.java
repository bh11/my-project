package bh11_class008;

import java.util.Arrays;

public class BH11_Class008_BubbleSort {

    public static void main(String[] args) {

        int[] testArray = {20, 9, 3, 16, -5, 66};

        for (int i = testArray.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (testArray[j] > testArray[j + 1]) {
                    int temp = testArray[j];
                    testArray[j] = testArray[j + 1];
                    testArray[j + 1] = temp;
                }
            }
        }
        for (int i = 0; i < testArray.length; i++) {
            System.out.print(testArray[i] + " ");
        }

        System.out.println();
        
        /// NEM BOUBBLE SORT, HANEM QUICK SORT

        int[] testArray2 = {20, 9, 3, 16, -5, 66};
        Arrays.sort(testArray2);
        

        for (int i = 0; i < testArray2.length; i++) {
            System.out.print(testArray2[i] + " ");
        }
        System.out.println();
        int i =Arrays.binarySearch(testArray2, 16);
        System.out.println(i);
    }
}
