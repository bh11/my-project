package bh11_class008;

import java.util.Scanner;

public class BH11_Class008_NewGame {

    static final int SIZE = 10;

    static final int NUMBER_OF_QUESTION_MARKS = 3;
    static final int NUMBER_OF_ASTERIX = 5;

    static final char QUESTION_MARK = '?';
    static final char ASTERIX = '*';
    static final char UP = 'w';
    static final char DOWN = 's';
    static final char LEFT = 'a';
    static final char RIGHT = 'd';
    static final char EMPTY = '\0';
    static final char PLAYER = '\u263A';

    static int playerX = 0;
    static int playerY = 0;
    static char[][] field = new char[SIZE][SIZE];

    static int getPosition() {
        return (int) (Math.random() * SIZE);
    }

    static void putElements(int count, char character, boolean isRepeatable) {
        for (int i = 0; i < count; i++) {
            int x = getPosition();
            int y = getPosition();

            if (isRepeatable) {
                if (field[x][y] == EMPTY || field[x][y] == character) {
                    field[x][y] = character;
                } else {
                    if (field[x][y] == EMPTY) {
                        field[x][y] = character;
                    } else {
                        i--;
                    }
                }

            }
        }
    }

    static void putPlayer() {
        field[playerX][playerY] = PLAYER;
    }

    static void printField(char[][] field) {

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (i == playerX && j == playerY) {
                    System.out.print(PLAYER);
                }else if (field[i][j] == ASTERIX || field[i][j] == QUESTION_MARK) {
                    System.out.print(field[i][j]);
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    static boolean anyCharacterxOnTheField(char character) {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] == character) {
                    return true;
                }
            }
        }
        return false;
    }

    static void play() {
        Scanner scanner = new Scanner(System.in);
        while (anyCharacterxOnTheField(QUESTION_MARK)) {
            printField(field);
            char c = scanner.next().charAt(0);
            switch (c) {
                case UP:
                    handleUp();
                    break;
                case DOWN:
                    handleDown();
                    break;
                case LEFT:
                    handleLeft();
                    break;
                case RIGHT:
                    handleRight();
                    break;

            }
        }
        scanner.close();
    }

    static void performStep() {
        if (field[playerX][playerY] == ASTERIX || field[playerX][playerY] == EMPTY || field[playerX][playerY] == PLAYER) {
            field[playerX][playerY] = PLAYER;

        } else if (field[playerX][playerY] == QUESTION_MARK && !anyCharacterxOnTheField(ASTERIX)) {
            field[playerX][playerY] = PLAYER;
        }
    }

    static void handleUp() {
        if (playerX != 0) {
            playerX--;
            performStep();
        }
    }

    static void handleDown() {
        if (playerX != field.length - 1) {
            playerX++;
            performStep();
        }
    }

    static void handleLeft() {
        if (playerY != 0) {
            playerY--;
            performStep();
        }
    }

    static void handleRight() {
        if (playerY != field[0].length - 1) {
            playerY++;
            performStep();
        }

    }

    public static void main(String[] args) {

        /*
        Adott egy 10x10-es 2D-s tömb
        5db * (több * lehet egy helyen)
        3db ? (egy ? csak egy helyen lehet)
        Fel kell venni az elemeket, viszont amíg van * a pályán, nem lehet kérdőjelet felvenni
         */
        putPlayer();
        putElements(NUMBER_OF_QUESTION_MARKS, QUESTION_MARK, false);
        putElements(NUMBER_OF_ASTERIX, ASTERIX, true);
        printField(field);
        play();

    }

}
