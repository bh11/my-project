
package bh11_homework_22_mvc;

import controller.Controller;
import model.EmployeeStore;
import view.ConsoleView;
import view.SwingView;
import view.View;


public class Main {

    public static void main(String[] args) {

     View v = new SwingView();
     View v2 = new ConsoleView();
     EmployeeStore m = new EmployeeStore();     
     Controller cont = new Controller(v,m);
     
     v.start();

    }
    
}
