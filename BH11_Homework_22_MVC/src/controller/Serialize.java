package controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

public class Serialize {

    private static final String FILENAME = "employee.txt";

    public static void save(List<Employee> emp) throws IOException {
        try (FileOutputStream fs = new FileOutputStream(FILENAME);
                ObjectOutputStream ou = new ObjectOutputStream(fs)) {
            ou.writeObject(emp);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
