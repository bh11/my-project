package controller;

import java.io.IOException;
import model.Employee;
import model.EmployeeFactory;
import model.EmployeeStore;
import view.View;

public class Controller {

    private final View view;
    private final EmployeeStore store;
    private Serialize s = new Serialize();

    public Controller(View view, EmployeeStore store) {
        this.view = view;
        this.store = store;
        view.setController(this);
    }
    

    public void handleButtonOrConsole(String name){
    Employee emp = EmployeeFactory.createEmployee(name);
    store.addEmployee(emp);    
    }
    
   public void listEmployee(){
       for (Employee emp: store.getEmployeeList()) {
           System.out.println(emp);
       }
   }
   
   public void saveEmployees() throws IOException{
   s.save(store.getEmployeeList());
           
   }

}
