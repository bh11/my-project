/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.util.Scanner;

/**
 *
 * @author gabri
 */
public class ConsoleView implements View {

    private static final String STOP = "Exit";
    private Controller controller;

    @Override
    public void setController(Controller c) {
        controller = c;
    }

    @Override
    public void start() {
        try (Scanner scanner = new Scanner(System.in)) {
            String inputText;
            do {
                inputText = scanner.nextLine();
                if (STOP.equalsIgnoreCase(inputText)) {
                    break;
                }

                controller.handleButtonOrConsole(inputText);
            } while (true);

        }
        System.out.println("Dolgozók listája: ");
        controller.listEmployee();
    }

}
