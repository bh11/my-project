/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.awt.BorderLayout;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author gabri
 */
public class SwingView extends JFrame implements View {

    private Controller controller;

    @Override
    public void setController(Controller c) {
        controller = c;
    }

    private final JButton jbAdd = new JButton("Hozzáad");
    private final JButton jbList = new JButton("Listáz");
    private final JButton jbSave = new JButton("Mentés");
    private final JTextField employeeText = new JTextField(25);

    @Override
    public void start() {
        buildView();
    }

    public void buildView() {
        jbAdd.addActionListener(l -> {
            controller.handleButtonOrConsole(employeeText.getText());
            employeeText.setText("");
        });
        jbList.addActionListener(l -> controller.listEmployee());

        jbSave.addActionListener(l -> {
            try {
                controller.saveEmployees();
            } catch (IOException ex) {
                Logger.getLogger(SwingView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        add(buildAddEmployee(), BorderLayout.NORTH);
        add(jbList, BorderLayout.CENTER);
        add(jbSave, BorderLayout.SOUTH);
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

    }

    public JPanel buildAddEmployee() {
        JLabel empLabel = new JLabel("Dolgozó neve:");
        JPanel emp = new JPanel();
        emp.add(empLabel);
        emp.add(employeeText);
        emp.add(jbAdd);
        return emp;
    }

}
