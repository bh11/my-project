package model;

import controller.Controller;
import java.util.ArrayList;
import java.util.List;

public class EmployeeStore {


    private Controller controller;
    private List<Employee> employeeList = new ArrayList<>();

    public void setController(Controller c) {
        controller = c;
    }
    
    public void addEmployee(Employee emp){
    employeeList.add(emp);
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }


}
