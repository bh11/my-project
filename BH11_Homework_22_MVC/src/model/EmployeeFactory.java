package model;

public class EmployeeFactory {

    public static Employee createEmployee(String name) {
        int bossOrWorker = (int) (Math.random() * 2);
        if (bossOrWorker == 0) {
            return new Boss(name);
        } else {
            return new Worker(name);
        }
    }

}
