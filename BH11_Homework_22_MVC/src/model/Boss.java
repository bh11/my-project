/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author gabri
 */
public class Boss implements Employee{

    private String name;
    private List<Worker> workers;

    public Boss(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public List<Worker> getWorkers() {
        return workers;
    }
    
    @Override
    public void setName(String name) {
        this.name = name;
    }

    public void setWorkers(List<Worker> workers) {
        this.workers = workers;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.name);
        hash = 79 * hash + Objects.hashCode(this.workers);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Boss other = (Boss) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.workers, other.workers)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Boss{" + "name=" + name + ", workers=" + workers + '}';
    }
    
    
}
