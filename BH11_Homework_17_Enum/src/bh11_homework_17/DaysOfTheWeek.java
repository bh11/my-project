package bh11_homework_17;

public enum DaysOfTheWeek implements Comparable<DaysOfTheWeek>{
    MONDAY(),
    TUESDAY(),
    WEDNESDAY(),
    THURSDAY(),
    FRIDAY(),
    SATURDAY(),
    SUNDAY();

    public boolean isHoliday() {
        return this == SATURDAY || this == SUNDAY;
    }

    public boolean isWeekday() {
        return !isHoliday();
    }

}
