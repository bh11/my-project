
package bh11_homework_23_generic;


public class Main {

 
    public static void main(String[] args) {

        Bag bag = new Bag();
        
        String a = "alma";
        String b = "barack";
        String c = "cseresznye";
        
        bag.add(a);
        bag.add(b);
        bag.add(c);
        
        bag.listElements();
    }
    
}
