package bh11_homework_23_generic;

import java.util.ArrayList;
import java.util.List;

public class Bag<T> {

    private List<T> list = new ArrayList<>();

    public void add(T element) {
        list.add(element);
    }

    public void listElements() {
        for (T t : list) {
            System.out.println(t);
        }
    }

}
