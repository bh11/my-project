package bh11_class014;

public class Pub {

    private int totalAmountOfWater;
    private int totalAmountOfWine;
    private final static int COST_OF_WATER = 50;
    private final static int COST_OF_WINE = 300;

    public Pub() {
    }

    public Pub(int totalAmountOfWater, int totalAmountOfWine) {
        this.totalAmountOfWater = totalAmountOfWater;
        this.totalAmountOfWine = totalAmountOfWine;
    }

    public Person serveDrink(Person person, Drink drink) {
        if (youngerThan18OrDrunk(person) || noWaterOrWineLeft(drink) || noMoneyLeft(person, drink)) {
            System.out.println("Nem tudunk kiszolgálni!");
        } else {
            this.totalAmountOfWater = totalAmountOfWater - drink.getWaterInDeciliters();
            this.totalAmountOfWine = totalAmountOfWine - drink.getWineInDecilites();
            person.setAlcoholLevel(person.getAlcoholLevel() + drink.howMuchAlcoholInTheDrink());
            int costOfDrink = drink.getWaterInDeciliters() * COST_OF_WATER + drink.getWineInDecilites() * COST_OF_WINE;
            person.setAmountOfMoney(person.getAmountOfMoney() - costOfDrink);
        }
        return person;
    }

    private boolean youngerThan18OrDrunk(Person person) {
        return person.getAge() < 18 || person.getAlcoholLevel() > 40;
    }

    private boolean noWaterOrWineLeft(Drink drink) {
        return this.totalAmountOfWater < drink.getWaterInDeciliters()
                || this.totalAmountOfWine < drink.getWineInDecilites();
    }

    private boolean noMoneyLeft(Person person, Drink drink) {
        return person.getAmountOfMoney() < drink.getWaterInDeciliters() * COST_OF_WATER + drink.getWineInDecilites() * COST_OF_WINE;
    }

    public int getWater() {
        return totalAmountOfWater;
    }

    public int getWine() {
        return totalAmountOfWine;
    }

    public void setWater(int water) {
        this.totalAmountOfWater = water;
    }

    public void setWine(int wine) {
        this.totalAmountOfWine = wine;
    }

    @Override
    public String toString() {
        return "Pub{" + "water=" + totalAmountOfWater + ", wine=" + totalAmountOfWine + '}';
    }

}
