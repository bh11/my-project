
package bh11_class014;


public class Drink {
    private int volumeOfDrink;
    private int waterInDeciliters;
    private int wineInDecilites;
    private final static double PERCENT_OF_ALCOHOL_IN_ONE_DL = 0.1;

    public Drink(int waterInDeciliters, int wineInDecilites){
    this.waterInDeciliters = waterInDeciliters;
    this.wineInDecilites = wineInDecilites;
    this.volumeOfDrink = waterInDeciliters+wineInDecilites;
    }
    
    public double howMuchAlcoholInTheDrink(){
    return wineInDecilites*PERCENT_OF_ALCOHOL_IN_ONE_DL;
    }
    public int getVolumeOfDrink() {
        return volumeOfDrink;
    }

    public void setVolumeOfDrink(int volumeOfDrink) {
        this.volumeOfDrink = volumeOfDrink;
    }

    public void setWaterInDeciliters(int waterInDeciliters) {
        this.waterInDeciliters = waterInDeciliters;
    }

    public void setWineInDecilites(int wineInDecilites) {
        this.wineInDecilites = wineInDecilites;
    }

    public int getWaterInDeciliters() {
        return waterInDeciliters;
    }

    public int getWineInDecilites() {
        return wineInDecilites;
    }


    @Override
    public String toString() {
        return "Drink{" + "volumeOfDrink=" + volumeOfDrink + ", waterInDeciliters=" + waterInDeciliters + ", wineInDecilites=" + wineInDecilites + '}';
    }
    
    
}
