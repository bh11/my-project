package bh11_class014;

public class Main {

    public static void main(String[] args) {
        
        Pub pub = new Pub(100,100);
        
        Person Gyula = new Person(50,20,7000);
        Person Bela = new Person(70,38,2000);
        
        Drink hazmester = new Drink(2,3);
        Drink kisfroccs = new Drink(1,1);
        Drink nagyfroccs = new Drink(1,2);
        Drink hosszulepes = new Drink(2,1);
        
        pub.serveDrink(Gyula, hazmester);
        pub.serveDrink(Bela, kisfroccs);
        pub.serveDrink(Bela, hosszulepes);
        pub.serveDrink(Bela, hazmester);
        pub.serveDrink(Gyula, kisfroccs);
        
        System.out.println(Bela);
        System.out.println(Gyula);
    }

}
