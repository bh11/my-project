
package bh11_class014;


public class Person {
    
    private int age;
    private int amountOfMoney;
    private double alcoholLevel;
    
    public Person (int age, int alcoholLevel, int amountOfMoney){
    this.age = age;
    this.alcoholLevel = alcoholLevel;
    this.amountOfMoney = amountOfMoney;
    }

    public int getAge() {
        return age;
    }

    public int getAmountOfMoney() {
        return amountOfMoney;
    }

    public double getAlcoholLevel() {
        return alcoholLevel;
    }

    public void setAmountOfMoney(int amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    public void setAlcoholLevel(double alcoholLevel) {
        this.alcoholLevel = alcoholLevel;
    }


    @Override
    public String toString() {
        return "Person{" + "age=" + age + ", amountOfMoney=" + amountOfMoney + ", alcoholLevel=" + alcoholLevel + '}';
    }
    
    
    
}
