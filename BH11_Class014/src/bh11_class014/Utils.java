package bh11_class014;

public class Utils {

    public static double getDrinkWithLowestAlcholContent(Drink[] drink) {
        
        double lowestAmountOfAlcohol = drink[0].howMuchAlcoholInTheDrink();

        for (int i = 0; i < drink.length; i++) {
            if (drink[i].howMuchAlcoholInTheDrink() < lowestAmountOfAlcohol) {
                lowestAmountOfAlcohol = drink[i].howMuchAlcoholInTheDrink();
            }
        }
        return lowestAmountOfAlcohol;
    }

    public static int getYoungestPerson(Person[] person){
        
    int youngestAge = person[0].getAge();
    
        for (int i = 0; i < person.length; i++) {
            if (person[i].getAge()<youngestAge) {
                youngestAge=person[i].getAge();
            }
        }
        return youngestAge;
    }


}
