
package hero;

import stone.AbstractStone;

public abstract class AbstractHero {

    private final String name;
    private final int power;
    private AbstractStone stone;
    
    public AbstractHero(String name, int power, AbstractStone stone){
    this.name = name;
    this.power = power;
    this.stone = stone;
    }

    public String getName() {
        return name;
    }

    public int getPower() {
        return power;
    }

    public AbstractStone getStone() {
        return stone;
    }

    public void setStone(AbstractStone stone) {
        this.stone = stone;
    }

    @Override
    public String toString() {
        return "AbstractHero{" + "name=" + name + ", power=" + power + ", stone=" + stone + '}';
    }
    
    
    
}
