
package bh11_homework_23.pkg18_drinks;

import read.ConsoleReader;
import report.Report;




public class Main {


    public static void main(String[] args) {
   
  ConsoleReader cr = new ConsoleReader();
  cr.read();
  Report report = new Report(); 
  report.report();
    }
    
}

/*
Create WINE Tokaji 0.2 3000 Hungarian
Create BEER Pilsener 0.2 600 German
Create PALINKA Hazi 0.6 6000 Hungarian
Create WINE Irsai 0.2 1000 Hungarian
Create WINE Kadarka 0.3 2000 Hungarian
Create PALINKA Panyolai 0.6 7000 Hungarian
Create BEER Dunkles 0.3 600 German
Create WINE Schlossgut 0.3 8000 German
Create WINE THIBAULT 0.6 10000 French
*/