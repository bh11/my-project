package read;

import drink.AbstractDrink;
import drink.DrinkFactory;
import report.Serialize;
import store.DrinkStore;

public class LineParser {

    private static final String DELIMETER = " ";
    private static final String CREATE = "CREATE";
    private static final String SAVE = "SAVE";
    private DrinkStore store = new DrinkStore();
    

    public void parseLine(String s) {
        String[] str = s.split(DELIMETER);
        if (CREATE.equalsIgnoreCase(str[0])) {
        create(str);
        }else if(SAVE.equalsIgnoreCase(str[0])){
        save();
        }
    }
    
    private void create(String[] parameters){
    AbstractDrink drink = DrinkFactory.createDrink(parameters);
        store.add(drink);
    }
    
    private void save(){
    Serialize s = new Serialize();
    s.serializeWines(store.getWineList());
    }

}
