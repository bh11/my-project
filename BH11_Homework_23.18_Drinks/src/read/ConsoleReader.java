package read;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConsoleReader {

    private static final int MAX_NUMBER_OF_LINES = 100;
    private static final String STOP = "Exit";
    private LineParser parser = new LineParser();

    private int counter;

    public void read() {

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            do {
                String s = br.readLine();
                if (s == null || STOP.equalsIgnoreCase(s)) {
                    break;
                }
                parser.parseLine(s);
                counter++;
            } while (counter != MAX_NUMBER_OF_LINES);

        } catch (IOException ex) {
            Logger.getLogger(ConsoleReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}


