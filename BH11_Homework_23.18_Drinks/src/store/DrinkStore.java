package store;

import drink.AbstractDrink;
import java.util.ArrayList;
import java.util.List;

public class DrinkStore {

    private static List<AbstractDrink> drinkList = new ArrayList<>();
    private static List<AbstractDrink> wineList = new ArrayList<>();

    public void add(AbstractDrink drink) {
        drinkList.add(drink);
        wineList(drink);
    }

    public void wineList(AbstractDrink drink) {
        if (drink.getType() == drink.getType().WINE) {
            wineList.add(drink);
        }
    }

    public List<AbstractDrink> getDrinkList() {
        return drinkList;
    }


    public List<AbstractDrink> getWineList() {
        return wineList;
    }

 
    
}
