
package drinkTypes;

import attributes.Attenuation;
import attributes.Drinkable;
import drink.AbstractDrink;
import drink.Producer;
import drink.Type;


public class Hungarian extends AbstractDrink implements Drinkable, Attenuation{
    
    public Hungarian(Type type, Producer producer, double alcoholContent, int price) {
        super(type, producer, alcoholContent, price);
    }

    @Override
    public void drink() {
        System.out.println("Ivás");
    }

    @Override
    public void attenuate() {
        System.out.println("Higítás");
    }
    
}
