
package drinkTypes;

import attributes.Color;
import drink.AbstractDrink;
import drink.Producer;
import drink.Type;

public class German extends AbstractDrink {
    
    private Color color;
    private String date;
    
    public German(Type type, Producer producer, double alcoholContent, int price) {
        super(type, producer, alcoholContent, price);
        if (alcoholContent<0.1) {
           color = color.GREEN;
        }else if(alcoholContent==0.5){
        color = color.YELLOW;
        }else{
        color = color.RED;
        }
        date = "1990.01.01.";
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return super.toString() + "German{" + "color=" + color + ", date=" + date + '}';
    }


    
}
