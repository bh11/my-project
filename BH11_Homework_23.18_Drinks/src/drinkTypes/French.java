
package drinkTypes;

import attributes.Congeable;
import attributes.Transportable;
import drink.AbstractDrink;
import drink.Producer;
import drink.Type;


public class French extends AbstractDrink implements Transportable, Congeable{

    public French(Type type, Producer producer, double alcoholContent, int price) {
        super(type, producer, alcoholContent, price);
        
    }

    @Override
    public void transport() {
        System.out.println("Szállítás");
    }

    @Override
    public void freeze() {
        System.out.println("Fagyasztás");
    }

    
    
    
}
