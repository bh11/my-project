
package report;


import drink.AbstractDrink;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import store.DrinkStore;

public class Serialize{
    
    private static final String FILE_NAME = "Drinks.txt";
    private DrinkStore store = new DrinkStore();
    
    public void serializeWines(List<AbstractDrink> wine){
        try (FileOutputStream fs = new FileOutputStream(FILE_NAME); 
                ObjectOutputStream ou = new ObjectOutputStream(fs)){
            ou.writeObject(wine);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Serialize.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
