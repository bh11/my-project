
package report;

import drink.AbstractDrink;
import drinkTypes.French;
import drinkTypes.German;
import drinkTypes.Hungarian;
import store.DrinkStore;


public class Report {
    
 private DrinkStore store = new DrinkStore();
  public void report(){
      serializedItems();
      System.out.println();
      countByDrinkTypes();
      System.out.println();
      wineList();
      System.out.println();
      producers();
  }
  
  //Hány termék lett kimentve?
  private void serializedItems(){
  int i = store.getWineList().size();
      System.out.printf("%d termék lett kimentve", i);
  }
  
  //Hány termék van speciális csoportonként?
 private void countByDrinkTypes(){
   countOfFrenchDrinks();
   countOfHungarianDrinks();
   countOfGermanDrinks();
 }
 
 private void countOfFrenchDrinks(){
 long frenchWines =store.getDrinkList().stream()
           .filter(p-> p instanceof French)
           .count();
     System.out.println("A francia italok száma: "+frenchWines);
 }
 
 private void countOfHungarianDrinks(){
  long hungarianWines = store.getDrinkList().stream()
             .filter(p->p instanceof Hungarian)
             .count();
      
     System.out.println("A magyar italok száma: "+hungarianWines);
 }
 
  private void countOfGermanDrinks(){
  long germanWines = store.getDrinkList().stream()
             .filter(p->p instanceof German)
             .count();
     System.out.println("A német italok száma: "+germanWines);
 }
  
  //Hány palackozott bor van készleten?
  private void wineList(){
  int i = store.getWineList().size();
  System.out.printf("%d palackozott bor van készleten", i);
  }
    
  
  //Kik a gyártók a készletet nézve (fel kell sorolni őket)?
  private void producers(){
  store.getDrinkList().stream()
          .map(m->m.getProducer())
          .forEach(System.out::println);
  }
}
