
package drink;

import drinkTypes.French;
import drinkTypes.German;
import drinkTypes.Hungarian;


public class DrinkFactory {
    
    private static final String FRENCH = "French";
    private static final String HUNGARIAN = "Hungarian";
    private static final String GERMAN = "German";
    
    public static AbstractDrink createDrink(String[] parameters){
    

        Type type = Type.valueOf(parameters[1]);
        Producer producer = new Producer(parameters[2]);
        double alcoholContent = Double.parseDouble(parameters[3]);
        int price = Integer.parseInt(parameters[4]);
        
        if (FRENCH.equalsIgnoreCase(parameters[5])) {
            return new French(type, producer, alcoholContent, price);
        }else if (HUNGARIAN.equalsIgnoreCase(parameters[5])){
        return new Hungarian(type, producer, alcoholContent, price);
        }else{
        return new German(type, producer, alcoholContent, price);
        }
    }
    
}
