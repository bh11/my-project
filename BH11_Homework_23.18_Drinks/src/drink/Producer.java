
package drink;

import java.io.Serializable;


public class Producer implements Serializable{
    
    String name;
    String adress;

    public Producer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    @Override
    public String toString() {
        return "Producer{" + "name=" + name + ", adress=" + adress + '}';
    }
    
    
 
}
