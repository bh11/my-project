
package drink;

import java.io.Serializable;



public abstract class AbstractDrink implements Serializable{
    
    /*
    Minden italnak van típusa (sör, bor, pálinka),
    gyártója (a gyártónak van neve és címe), 
    vonalkódja (5jegyű generált szám), alkoholtartalma és ára (int). 
    */
    Type type;
    Producer producer;
    private int barcode = (int)(Math.random()*10000);
    private double alcoholContent;
    private int price;

    public AbstractDrink(Type type, Producer producer, double alcoholContent, int price) {
        this.type = type;
        this.producer = producer;
        barcode++;
        this.alcoholContent = alcoholContent;
        this.price=price;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public int getBarcode() {
        return barcode;
    }

    public void setBarcode(int barcode) {
        this.barcode = barcode;
    }

    public double getAlcoholContent() {
        return alcoholContent;
    }

    public void setAlcoholContent(double alcoholContent) {
        this.alcoholContent = alcoholContent;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "AbstractDrink{" + "type=" + type + ", producer=" + producer + ", barcode=" + barcode + ", alcoholContent=" + alcoholContent + ", price=" + price + '}';
    }
    
    

    
    
}
