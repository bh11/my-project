
package bh11_class016;

import reader.ConsoleReader;
import report.Reporting;
import store.StoreAdd;
import store.StringStore;


public class Main {

    public static void main(String[] args) {
        // TODO code application logic here
   
    /*
        A felhasználó szavakat ad meg az exit szóig, ezeket a szavakat tároljuk, 
        3 műveletet hajtunk végre: 
        -hány szót adott meg és melyek ezek?
        -írjuk ki a megadott szavakat fordított sorrendben
        -szavak hossza szerint csoportosítsuk a szavakat
        */
        ConsoleReader reader = new ConsoleReader();
        StringStore store = new StringStore();
        reader.readByScanner(store);
        Reporting report = new Reporting(store);
        
        
        report.generateReports();
    
    
    }
    
}
