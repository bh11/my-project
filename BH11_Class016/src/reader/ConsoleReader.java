package reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import store.StoreAdd;
import store.StringStore;

public class ConsoleReader {

    private static final String STOP = "exit";

    public void readByScanner(StoreAdd store) {
        try (Scanner scanner = new Scanner(System.in)) {
            String word = null;
            do {
                word = scanner.next();
                store.add(word);
            } while (!STOP.equals(word));
        }
    }

    public void readByInputStringReader(StoreAdd store) {
        BufferedReader br = null;

        try {
            br = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                String line = br.readLine();
            }
        } catch (IOException ex) {
            System.out.println(ex);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    System.out.println(ex);
                }

            }

        }

    }

}
