package report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import store.GetList;

public class Reporting {

    private final List<String> data;

    public Reporting(GetList store) {
        this.data = store.getData();
    }

    public void generateReports() {
        reportUniqueWords();
        reportReversedItems();
        reportGroupedItems();
    }

    private void reportUniqueWords() {
        System.out.println("*************************************");
        Set<String> words = new HashSet<>(data);
        System.out.println("Egyedi szavak száma: " + words.size());
        System.out.println("Egyedi szavak: ");

        for (String s : words) {
            System.out.println(s);
        }
    }

    private void reportReversedItems() {
        System.out.println("*************************************");
        System.out.println("A szavak forsított sorrendben:");

        for (int i = data.size()-1; i >= 0; i--) {
            System.out.println(data.get(i));
        }
    }
    private void buildGroupedItems(Map<Integer, List<String>> map){
      for (String i : data) {
          int key = i.length();
          if (map.get(key) == null) { //VAGY if(!map.containsKey(key))
              map.put(key, new ArrayList<>());
          }
            map.get(key).add(i);
        }
    }
    private void reportGroupedItems(){
    System.out.println("*************************************");
    Map<Integer, List<String>> wordByLength = new HashMap<>();
        buildGroupedItems(wordByLength);
        
        for(Integer i : wordByLength.keySet()){
        System.out.println(i + " szavak: "+ wordByLength.get(i));
    }
    }
}
