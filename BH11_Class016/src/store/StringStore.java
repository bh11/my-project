package store;

import java.util.ArrayList;
import java.util.List;

public class StringStore implements StoreAdd, StoreRemove, GetList{

    private List<String> strings = new ArrayList<>();

    @Override
    public void add(String s) {
        strings.add(s);
        System.out.println(s + " added");
    }
   
    @Override
    public void remove (String s){
    strings.remove(s);
    }

    @Override
    public List<String> getData() {
       return strings;
    }
    
}
