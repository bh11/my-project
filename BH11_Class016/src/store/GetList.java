
package store;

import java.util.List;

public interface GetList {
    
    List<String> getData();
}
