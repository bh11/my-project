
public enum DaysOfTheWeek {

    MONDAY{
    @Override
    public void todaysLine(){
        System.out.println("Nyeh, megint hétfő");
    }
    },
    TUESDAY{
    @Override
    public void todaysLine(){
        System.out.println("Már csak négy nap szombatig");
    }
    },
    WEDNESDAY{
    @Override
    public void todaysLine(){
        System.out.println("Szerdaaaaaa");
    }
    },
    THURSDAY{
    @Override
    public void todaysLine(){
        System.out.println("Kispéntek");
    }
    },
    FRIDAY{
    @Override
    public void todaysLine(){
        System.out.println("Hamarabb mehetünk ma haza");
    }
    },
    SATURDAY{
    @Override
    public void todaysLine(){
        System.out.println("Hétvégeeeeeeeee");
    }
    },
    SUNDAY{
    @Override
    public void todaysLine(){
        System.out.println("Holnap hétfő :( ");
    }
    };

public abstract void todaysLine();

}


