package bh11_class010;

public class Company {

    private Employee[] employees;
    private int numberOfEmployees;
    public static double raiseIndicator = 1.1;

    Company() {
        employees = new Employee[10];
    }

    public void addEmployee(Employee emp) {
        //increaseMaximumNumberOfEmployees
        if (numberOfEmployees == employees.length) {
        increseMaximumNumberOfEmployees();
        }else{
        employees[numberOfEmployees] = emp;
        numberOfEmployees++;
    }
    }
    
    private void increseMaximumNumberOfEmployees(){
    Employee[] employeesTemp = new Employee[numberOfEmployees * 2];
        for (int i = 0; i < employees.length; i++) {
            employees[i]=employeesTemp[i];
        }
      employees = employeesTemp;
    }

    public void increaseSalaries() {
        for (int i = 0; i < numberOfEmployees; i++) {
            employees[i].increseSalary(raiseIndicator);
        }
    }

    public void printEmployesWhithSalaries() {
        for (int i = 0; i < numberOfEmployees; i++) {
            System.out.println(employees[i].getName().getFullName() + "'s salary " + employees[i].getSalary());
        }
    }

}
