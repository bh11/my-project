package bh11_class010;

public class Employee {

    private Name name;
    private double salary;

   public Employee(String firstName, String lastName, double salary) {
        name = new Name(firstName, lastName);
        this.salary = salary;
    }

//Erre azért van szükség, hogy azok, akiknek két keresztnevük van, megadhassák a középső nevüket
//    Employee(String firstName, String middleName, String lastName, double salary) { //Konstruktor túlterhetlés
//        this.firstName = firstName;
//        this.middleName = middleName;
//        this.lastName = lastName;
//        this.salary = salary;
//    }
    //Konstruktorok meghívása egymásból:
   public  Employee(String firstName, String middleName, String lastName, double salary) { //Konstruktor túlterhetlés
        this(firstName, lastName, salary);   // Arra a kontruktorra hivatkozunk, aminek ez a paraméterlistája
        name.setMiddleName(middleName);
    }

   public  Employee(Name name, double salary) {
        this.name = name;
        this.setSalary(salary);
    }

    public void increseSalary(double raiseIndicator) {
        salary = salary * raiseIndicator;
    }

    // Ezt az App-ban úgy adom meg, hogy emloyee6.getFullName    
//    String getFullName() {
//        return name.getFullName();
//    }
    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        if (salary < 0 || salary > 10000000) {
            this.salary = 0;
            System.out.println("Wrong salary, salary is set to 0");
        } else {
            this.salary = salary;
        }
    }
}
