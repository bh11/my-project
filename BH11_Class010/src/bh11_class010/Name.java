package bh11_class010;

public class Name {

    private String firstName;
    private String lastName;
    private String middleName;

    public Name(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Name(String firstName, String middleName, String lastName) {
        this(firstName, lastName);
        this.middleName = middleName;
    }

    public String getFullName() {
        if (this.middleName == null) {
            return firstName + " " + lastName;
        } else {
            return firstName + " " + middleName + " " + lastName;
        }

    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

     public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
     
      public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
