
package bh11_class013_string;


public class BH11_Class013_String {


    public static void main(String[] args) {

        
        String s1 = "Alma";
        s1.toLowerCase();  
        System.out.println(s1);
        /*
        Mivel nem raktuk bele egy másik változóba, és a String immutable, ezért
        a tényleges értéke nem fog változni
        */
        
        String s2 = " alma És Nyúl  ";
        System.out.println(s2.trim().toLowerCase());
        /*
        Egymásba láncolva meg lehet hívni a Stringek metódusat (chain). 
        Ha ki akarjuk írni a nélkül, hogy másik változóba elmentenénk, rögtön a 
        sout-ba kell raknunk.
        */
        
        String s3 = "Hello";
        String s4 = "Hello";
        
        if (s3==s4.trim()) {
            System.out.println("true");
        }else{
            System.out.println("false");
        }
        /*
        Mivel a trim nem talál wide space-eket a szó elején vagy a végén, ezért
        egyszerűen összehasonlítja a két Stringet
        */
        String s5 = "Hello ";
        String s6 = "Hello ";
        
        if (s5==s6.trim()) {
            System.out.println("true");
        }else{
            System.out.println("false");
        }
        /*
        Hiába ugyanaz a két String, a space-k levágása után nem kerülnek bele a 
        String Poolba, ezért a két Stringet nem látja ugyanannak.
        */
        
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder(10);
        StringBuilder sb3 = new StringBuilder("alma");
        
        StringBuilder sb4 = sb3.append(1); //itt az sb3 értéke is megváltozik!!!
        System.out.println(sb3);
        System.out.println(sb4);
        
        String str = " alma Körte\t";
        System.out.println("|"+ str + "|");
        System.out.println("|"+ str.trim() + "|");
        System.out.println("|"+ str.trim().replace('a', 'x') + "|");
        System.out.println("|"+ str.trim().replace('a', 'x').indexOf('a') + "|"); //mivel már nincsenek benne a-k
        System.out.println("|"+ str.trim().replace('a', 'x').indexOf('t') + "|");
                
    }
    
}
