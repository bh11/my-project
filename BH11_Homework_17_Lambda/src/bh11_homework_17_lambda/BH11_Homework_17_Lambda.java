package bh11_homework_17_lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class BH11_Homework_17_Lambda {

    public static void main(String[] args) {
        /*
        Basic lambdas. Make an array containing a few Strings. Sort it by
• length (i.e., shortest to longest)
• reverse length (i.e., longest to shortest)
• alphabetically by the first character only
• Strings that contain “e” first, everything else second. For now, put the code directly in the lambda.
         */

        List<String> str = new ArrayList<>();

        str.add("kókuszmókus");
        str.add("pókmalac");
        str.add("mazsola");
        str.add("epermackó");
        str.add("pocoknyúl");

        System.out.println("Sorbarendezés hossz alapján:");
        Collections.sort(str, (String x, String y) -> x.length() - y.length());
        str.forEach(s -> System.out.println(s));

        System.out.println();
        System.out.println("Sorbarendezés (hossz) visszafelé:");

        Collections.sort(str, (String x, String y) -> y.length() - x.length());
        str.forEach(s -> System.out.println(s));

        System.out.println();
        System.out.println("Sorbarendezés abc sorrendben:");
        Collections.sort(str, (String x, String y) -> x.charAt(0) - y.charAt(0));
        str.forEach(s -> System.out.println(s));

        System.out.println();
        System.out.println("Sorbarendezés az alapján, hogy tartalmaz-e e betűt");
        Collections.sort(str, (String x, String y) -> (x.contains("e") ? 0 : 1) - (y.contains("e") ? 0 : 1));
        str.forEach(s -> System.out.println(s));
        
        

    }
}
