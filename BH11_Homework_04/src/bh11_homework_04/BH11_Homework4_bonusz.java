package bh11_homework_04;

import java.util.Scanner;

public class BH11_Homework4_bonusz {

    public static void main(String[] args) {
        /*
        Bónusz: 
    bekérünk egy felhasználótol egy számot, váltsuk át kettes számrendszerbe
         */
        Scanner scanner = new Scanner(System.in);
        boolean asking = true;
        int number = 0;
        System.out.println("Kérlek adj meg egy egész számot!");
        do {
            if (scanner.hasNextInt()) {
                number = scanner.nextInt();
                asking = false;
            } else {
                scanner.next();
            }
        } while (asking);

        int index = 0;
        int[] array = new int[1024];

        for (int i = 0; number > 0;) {
            if (number % 2 == 0) {
                array[i] = 0;
                number = number / 2;
                i++;
                index++;
            } else {
                array[i] = 1;
                number = number / 2;
                i++;
                index++;
            }
        }

        int[] array2 = new int[index];
        for (int i = 0; i < array2.length; i++) {
            array2[i] = array[i];
        }

        System.out.print("Kettes számrendszerben: ");
        for (int i = array2.length - 1; i >= 0; i--) {
            System.out.print(" " + array2[i]);
        }
        System.out.println();
    }

}
