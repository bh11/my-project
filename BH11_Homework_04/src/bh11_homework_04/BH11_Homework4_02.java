package bh11_homework_04;

import java.util.Scanner;

public class BH11_Homework4_02 {

    public static void main(String[] args) {
        /*
        2. 
        kérjük be egy pozitív egész számot (ellenőrzés), 
        majd írjunk ki csillagokat az alábbi formában a megadott szám alapján: 
        a. 
        szam = 7
        *******
        ******
        *****
        ****
        ***
        **
        *
        b. 
        szam = 5 (ha páros a szám, vonjunk ki belőle egyet)
          *
         ***
        *****
         */

        Scanner scanner = new Scanner(System.in);
        System.out.println("Kérlek irj be egy pozitív egész számot!");
        boolean asking = true;
        int number = 0;
        do {
            if (scanner.hasNextInt()) {
                number = scanner.nextInt();
                if (number > 0) {
                    asking = false;
                }
            } else {
                scanner.next();
            }
        } while (asking);

        for (int i = number; i >= 0; i--) {
            for (int j = 1; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }

        for (int i = 0; i < number; i++) {
            if (i % 2 == 0) {
                for (int j = number - i; j > 1; j--) {
                    System.out.print(" ");
                }
                for (int j = 0; j <= i; j++) {
                    System.out.print("* ");
                }
                System.out.println();
            }
        }
    }
}
