package bh11_homework_04;

public class BH11_Homework4_01 {

    public static void main(String[] args) {

        /*
        1. tomb, 50 elem, [-100, 100] értékkészlet, írjuk ki a következőket: 
        - maximum hely és érték,
         - minimum hely és érték, 
         - átlag 
        - összeg
         */
        int[] arr = new int[50];
        int minValue = arr[0];
        int minIndex = 0;
        int maxValue = arr[0];
        int maxIndex = 0;
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) ((Math.random() * 200) - 100);
            if (minValue > arr[i]) {
                minValue = arr[i];
                minIndex = i;
            }
            if (maxValue < arr[i]) {
                maxValue = arr[i];
                maxIndex = i;
            }
            sum += arr[i];
        }

        System.out.print("A tömb elemei: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        System.out.println("Maximum hely: " + maxIndex);
        System.out.println("Maximum érték: " + maxValue);
        System.out.println("Minimum hely: " + minIndex);
        System.out.println("Minimum érték: " + minValue);
        System.out.println("A tömb értékeinek összege: " + sum);
        System.out.println("A tömb értékeinek átlaga " + sum / arr.length);
    }

}
