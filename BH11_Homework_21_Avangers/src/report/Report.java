package report;

import hero.AbstractHero;
import hero.BornOnEarth;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import read.LineParser;
import stone.StoneType;
import store.Fleet;
import store.Ship;

public class Report {

    private final Fleet fleet;

    public Report() {
        this.fleet = LineParser.getStore();
    }

    public void generateReport() {
        reportNumberOfHeroesBornOnTheEarth();
        reportPassportNumbers();
        reportStrongestHero();
        printCountOfStoneByShips();
    }

    public void reportNumberOfHeroesBornOnTheEarth() {
        long numberOfHeroes = fleet.getAllHeroesInTheShips().stream()
                .filter(e -> e instanceof BornOnEarth)
                .count();
        System.out.println("A földön születettek száma: " + numberOfHeroes);
    }

    public void reportStrongestHero() {
        OptionalInt heroPower = fleet.getAllHeroesInTheShips().stream()
                .mapToInt(h -> h.getPower())
                .max();

        if (heroPower.isPresent()) {
            System.out.println("A legerősebb Bosszúálló ereje: " + heroPower.getAsInt());
        }

    }

    public void reportPassportNumbers() {

        fleet.getAllHeroesInTheShips().stream()
                .filter(h -> h instanceof BornOnEarth)
                .map(hero -> ((BornOnEarth) hero).getIdentityCard())
                .forEach(System.out::println);
    }
    
    private void printCountOfStoneByShips(){
        countOfStoneByShips().entrySet().stream()
                .forEach(i->{System.out.println("Ship: "+i);
                i.getValue()
                        .forEach((j, k)-> System.out.println("Stone: "+j+" Count: "+k));
                });
        
    }

    private Map<StoneType, Integer> countOfStones(Ship ship) {
        return ship.getHeroes().stream()
                .map(AbstractHero::getStone)
                .collect(Collectors.toMap(
                        k -> k,
                        v -> 1,
                        (v1, v2) -> v1 + v2,
                        HashMap::new
                )
                ); //A HashMap a defalult, ebben az esetben nem szükséges kitenni
    }

    private Map<Ship, Map<StoneType, Integer>> countOfStoneByShips() {
        return fleet.getShips().stream()
                .collect(Collectors.toMap(
                        s -> s,
                        this::countOfStones, //  v-> countOfStones(v)
                        (x, y) -> x //Ha ütközés van két kulcs között, tartsa meg a régit
                )
                );
    }
    
    private boolean containsOnlyBornOnEarth(Ship ship){
        return ship.getHeroes().stream()
                .allMatch(h->h instanceof BornOnEarth);
    }
    
    private Map<Boolean, List<Ship>> partitioningByContainsOnlyBornOnEarth(){
    return fleet.getShips().stream()
            .collect(Collectors.partitioningBy(this::containsOnlyBornOnEarth));
    }
    
    private long numberOfShipsCountainingOnlyBornOnHeroes(){
    return partitioningByContainsOnlyBornOnEarth().get(Boolean.TRUE).size();
    }

        public void reportIntoFile() throws IOException {
        try (FileWriter fw = new FileWriter("Avangers.txt");
                PrintWriter pw = new PrintWriter(fw)) {
            List<AbstractHero> heroes = new ArrayList<>();
            heroes.addAll(fleet.getAllHeroesInTheShips());
            
            heroes.forEach(pw::println);
        }
    }
       

}
