package store;

import hero.AbstractHero;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Ship {

    public static final int MAX_SIZE = 4;
    public static final int INDEX_IN_NAME = 1;
    private final List<AbstractHero> heroes = new ArrayList<>();

    public void add(AbstractHero hero) {
        if (isEmptySeat()) {
            heroes.add(hero);
        }
        orderBySecondCharacterOfName();
    }

    private void orderBySecondCharacterOfName() {
//Anonymus osztály létrehozásával
        Comparator<AbstractHero> cmp = new Comparator<AbstractHero>() {
            @Override
            public int compare(AbstractHero hero1, AbstractHero hero2) {
                return hero1.getName().charAt(INDEX_IN_NAME) - hero2.getName().charAt(INDEX_IN_NAME);
            }
        };
        Collections.sort(heroes, cmp);
    }

    private void orderBySecondCharacterOfName2() {
        Collections.sort(heroes, new Comparator<AbstractHero>() {
            @Override
            public int compare(AbstractHero hero1, AbstractHero hero2) {
                return hero1.getName().charAt(INDEX_IN_NAME) - hero2.getName().charAt(INDEX_IN_NAME);
            }
        });
    }

    private void orderBySecondCharacterOfName3() {
        Collections.sort(heroes, (AbstractHero hero1, AbstractHero hero2) -> {
            return hero1.getName().charAt(INDEX_IN_NAME) - hero2.getName().charAt(INDEX_IN_NAME);
        });
    }

    private void orderBySecondCharacterOfName4() {
        Collections.sort(heroes, (hero1, hero2)
                -> hero1.getName().charAt(INDEX_IN_NAME) - hero2.getName().charAt(INDEX_IN_NAME));
    }

    public List<AbstractHero> getHeroes() {
        return heroes;
    }


    public boolean isEmptySeat() {
        return heroes.size() < MAX_SIZE;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.heroes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ship other = (Ship) obj;
        if (!Objects.equals(this.heroes, other.heroes)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Ship{" + "heroes=" + heroes + '}';
    }
}
