
package bh11_class019_serialization;

import java.io.Serializable;


public class Department implements Serializable{
    
    private String address;
    private String name;

    public Department(String address, String name) {
        this.address = address;
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Department{" + "address=" + address + ", name=" + name + '}';
    }
    
    
    
}
