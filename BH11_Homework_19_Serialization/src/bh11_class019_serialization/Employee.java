
package bh11_class019_serialization;

import java.io.Serializable;


public class Employee implements Serializable {
    
    private int age;
    private int salary;
    private String name;
    private Department department;

    public Employee(int age, int salary, String name, Department department) {
        this.age = age;
        this.salary = salary;
        this.name = name;
        this.department = department;

    }

    public int getAge() {
        return age;
    }

    public int getSalary() {
        return salary;
    }

    public String getName() {
        return name;
    }

    public Department getDepartment() {
        return department;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Employee{" + "age=" + age + ", name=" + name + ", department=" + department + '}';
    }
    
    
}
