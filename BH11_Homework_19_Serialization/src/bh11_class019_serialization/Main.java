
package bh11_class019_serialization;


public class Main {


    public static void main(String[] args) {
        
        /*
        EmployeeRepository-t kiegészíteni úgy, hogy képes legyen Employee-kat tárolni (többet is)
Ezekhez az Employee-khoz lehessen-hozzáadni
-törölin
-update-elni
-listázni őket
-lehessen valamilyen azonosító alapján elkérni egy employee-t
-tudjon department szerint listázni
-az egész perzisztens legyen, tehát a már beleadott employee-k benne legyenek a futás után -- szerializálni kell
-minden legyen az App-ban letesztelve
        */

        Employee emp = new Employee(17, 200000, "Empi", new Department("Kis utca", "IT"));
        Employee emp2 = new Employee(30, 600000, "Employee", new Department("Lajos fasor", "Pénzügy"));
        Employee emp3 = new Employee(40, 500000, "Emp", new Department("Kis utca", "Ügyvitel"));
        EmployeeRepository.addEmployee(emp);
        EmployeeRepository.addEmployee(emp2);
        EmployeeRepository.addEmployee(emp3);
        
        
        // törlés
        EmployeeRepository.getEmplist().remove(emp);
        
        //update??? Mit takar pontosan?        
        
        //kilistázás
        EmployeeRepository.getEmplist().stream()
                .forEach(System.out::println);
        
        //azonosító - név szerint
        
        EmployeeRepository.getEmplist()
                .stream()
                .filter(e-> "Emp".equals(e.getName()))
                .forEach(System.out::println);
        
        //department szerinti lista
        
        EmployeeRepository.getEmplist()
                .stream()
                .map(employee-> employee.getDepartment())
                .forEach(System.out::println);
    
        
        //szerializálás
        EmployeeRepository.serializeEmployeeList(EmployeeRepository.getEmplist());
        
    }
    
}
