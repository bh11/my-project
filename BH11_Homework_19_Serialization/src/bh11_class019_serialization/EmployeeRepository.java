package bh11_class019_serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EmployeeRepository{

    private static final String FILENAME = "employee.txt";
    private static final String FILENAME_LIST = "employeeList.txt";
    private static List<Employee> emplist = new ArrayList<>();
    
    public static void addEmployee(Employee emp){
    emplist.add(emp);
    }

    public static void serializeEmployee(Employee emp) {
        try (FileOutputStream fs = new FileOutputStream(FILENAME);
                ObjectOutputStream ou = new ObjectOutputStream(fs)) {
            ou.writeObject(emp);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
        public static void serializeEmployeeList(List<Employee> emp) {
        try (FileOutputStream fs = new FileOutputStream(FILENAME_LIST);
                ObjectOutputStream ou = new ObjectOutputStream(fs)) {
            ou.writeObject(emp);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static Employee deSerializeEmployee() {
        Employee emp = null;

        try (
                FileInputStream fs = new FileInputStream(FILENAME);
                ObjectInputStream oi = new ObjectInputStream(fs)) {
            emp = (Employee) oi.readObject();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        return emp;
    }
    

    public static List<Employee> getEmplist() {
        return emplist;
    }

    public static void setEmplist(List<Employee> emplist) {
        EmployeeRepository.emplist = emplist;
    }
    
    
}
