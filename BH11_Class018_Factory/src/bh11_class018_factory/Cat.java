package bh11_class018_factory;

public class Cat extends AbstractAnimal {

    @Override
    public void sayHello() {
        System.out.println("Meow");
    }

}
