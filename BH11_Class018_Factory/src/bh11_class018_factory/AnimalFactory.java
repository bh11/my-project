package bh11_class018_factory;

public class AnimalFactory {

    public static AbstractAnimal create(String type) {
        if ("Cat".equals(type)) {
            return new Cat();
        } else if ("Dog".equals(type)) {
            return new Dog();
        } else if ("Snake".equals(type)) {
            return new Snake();
        }
        return null;
    }

}
