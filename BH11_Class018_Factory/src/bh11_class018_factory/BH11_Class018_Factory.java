package bh11_class018_factory;

public class BH11_Class018_Factory {

    public static void main(String[] args) {

        AbstractAnimal cat = AnimalFactory.create("Cat");
        AbstractAnimal dog = AnimalFactory.create("Dog");
        AbstractAnimal snake = AnimalFactory.create("Snake");
        cat.sayHello();
        dog.sayHello();
        snake.sayHello();
    }

}
