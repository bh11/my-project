
package bh11_class015_setlist;

import java.util.ArrayList;
import java.util.List;


public class BH11_Class015_SetList {

    public static boolean isPrime(int number){
        int counter = 0;
        for (int i = 1; i < number; i++) {
            if (number%i==0) {
            counter++;    
            }
        }
        
    if (counter>1 || number==1 || number==0) {
            return false;
        }else{
        return true;
    }
    }
    
    public static void main(String[] args) {

        
        List<Integer> primenumbers = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            if (isPrime(i)) {
                primenumbers.add(i);
            }
        }
        System.out.println(primenumbers);
    
        for (int n : primenumbers){
            System.out.println(n);
        }
    }
    
    
    
}
