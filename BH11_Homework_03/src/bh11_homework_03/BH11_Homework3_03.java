
package bh11_homework_03;

import java.util.Scanner;


public class BH11_Homework3_03 {
    public static void main(String[] args) {
        
        /*
        3. Kérj be egy pozitív egész számot (addig kérjünk be adatot, 
        amíg pozitív számot nem kapunk). Írd ki 0-tól az adott számig a számok 
        négyzetét. Pl.: bemenet: 4 - kimenet: 0 1 4 9 16
        */
        
        Scanner scanner = new Scanner(System.in);
        boolean asking = true;
        int number = 0;
        do {
            System.out.println("Kérlek írj be egy pozitív egész számot!");
            if (scanner.hasNextInt()) {
                number = scanner.nextInt();
                if (number>0) {
                   asking = false; 
                }   
            }else{
            scanner.next();
            } 
        } while (asking);

        int counter=0;
        do {
            System.out.println((int)(Math.pow(counter, 2)));
            counter++;
            
        } while (counter<=number);
        
    }
    
}
