package bh11_homework_03;

public class BH11_Homework3_06 {

    public static void main(String[] args) {
        /*
        6. Írd ki a páros számokat 55 és 88 között. 
        Oldd meg ezt a feladatot while és for felhasználásával is.
         */
        int num1 = 55;
        int num2 = 88;

        for (int i = num1; i < num2; i++) { //feltételezve, hogy a 88 már nem számít bele
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }

        }
        System.out.println();
        while (num1 < num2) {
            if (num1 % 2 == 0) {
                System.out.print(num1 + " ");
            }
            num1++;

        }

    }

}
