
package bh11_homework_03;

import java.util.Scanner;


public class BH11_Homework3_08 {
    public static void main(String[] args) {
        
        /*
        8. Olvass be 2 számot. Határzod meg, hogy a 2 szám által meghatározott koordináta melyik síknegyedben van.
    X≥0 és Y≥0 → Síknegyed=1
    X≥0 és Y<0 → Síknegyed=4
    X<0 és Y≥0 → Síknegyed=2
    X<0 és Y<0 → Síknegyed=3
        */
        Scanner scanner = new Scanner (System.in);
        int x = 0;
        int y = 0;
        boolean asking = true;
        
        do {
            System.out.println("Adj meg egy számot!");
            if (scanner.hasNextInt()) {
                x = scanner.nextInt();
                asking = false;
            } else {
            scanner.next();
            }
        } while (asking);
        
        asking = true;
        
         do {
            System.out.println("Adj meg még egy számot!");
            if (scanner.hasNextInt()) {
                y = scanner.nextInt();
                asking = false;
            } else {
            scanner.next();
            }
        } while (asking);
         
         if (x>=0 && y>=0) {
            System.out.println("Síknegyed=1");
        } else if(x>=0 && y<0){
              System.out.println("Síknegyed=4");  
        } else if (x<0 && y>=0){
              System.out.println("Síknegyed=2"); 
        } else if (x<0 && y<0){
              System.out.println("Síknegyed=3");
        }
           
    }
    
}
