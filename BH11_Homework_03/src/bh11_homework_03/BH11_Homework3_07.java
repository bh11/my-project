
package bh11_homework_03;

import java.util.Scanner;


public class BH11_Homework3_07 {
    public static void main(String[] args) {
        /*
        Kérj be egy számot a felhasználótól. Ha 7-tel osztható, írd ki, 
        hogy "kismalac". Ezt a feladatot oldd meg if és switch felhasználásával is.
        */
        
        Scanner scanner = new Scanner(System.in);
        int number = 0;
        boolean asking = true;
        System.out.println("Adj meg egy egész számot!");
        
        do {
            if (scanner.hasNextInt()){
            number = scanner.nextInt();
            asking = false;
            } else {
            scanner.next();
            }
        } while (asking);
        
        if (number % 7 == 0) {
            System.out.println("Kismalac");
        } else {
            System.out.println("A szám nem osztható 7-tel");
        }
        
        int maradek = number%7;
        
        switch (maradek){
            case 0: System.out.println("Kismalac"); break;
            default: System.out.println("A szám nem osztható 7-tel");
        }
    }
    
}
