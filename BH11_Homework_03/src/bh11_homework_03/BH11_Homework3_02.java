
package bh11_homework_03;

public class BH11_Homework3_02 {
    public static void main(String[] args) {
        
        /*
        2. Sorsolj ki egy egész számot az [1000;10000] intervallumból. Írd ki az első és utolsó számjegyét!
        */
        int random = (int) (Math.random()*(10000-1000+1)+1000);
        System.out.println(random);
        int firstDigit;
        int lastDigit = (int)(random % 10);
        
        firstDigit = random;
        while (firstDigit>=10){
                firstDigit /= 10;
        }
        System.out.println("Első számjegy: "+ firstDigit);
        System.out.println("Utolsó számjegy: "+ lastDigit);
    }
    
}
