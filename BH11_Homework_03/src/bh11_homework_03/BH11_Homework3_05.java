package bh11_homework_03;

import java.util.Scanner;

public class BH11_Homework3_05 {

    public static void main(String[] args) {

        /*
        5. Írj programot, mely addig olvas be számokat a billentyűzetről, 
        ameddig azok kisebbek, mint tíz. Írd ki ezek után a beolvasott számok összegét!
         */
        Scanner scanner = new Scanner(System.in);

        int number = 0;
        int counter = 0;

        System.out.println("Adj meg 10-nél kisebb számokat!");

        while (number < 10) {
            if (scanner.hasNextInt()) {
                number = scanner.nextInt();
            }else{
            scanner.next();
            }     
            if (number < 10) {
                counter += number;
            }
        }
        System.out.println("A számok összege: " + counter);
    }

}
