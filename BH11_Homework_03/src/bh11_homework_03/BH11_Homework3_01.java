package bh11_homework_03;

import java.util.Scanner;

public class BH11_Homework3_01 {

    public static void main(String[] args) {
        /*
        1. Kérj be egy számot, írd ki a páros osztóit!
         */

        Scanner scanner = new Scanner(System.in);
        int number = 0;
        int divider = 1;
        boolean asking = true;

        do {
            System.out.println("Kérlek írj be egy egész számot!");
            if (scanner.hasNextInt()) {
                number = scanner.nextInt();
                if (number>0) {
                  asking = false;  
                }   
            } else {
                scanner.next();
            }

        } while (asking);

        do {
            if (number % divider == 0) {
                if (divider % 2 == 0) {
                    System.out.println(divider);
                }
            }
            divider++;

        } while (number > divider);

    }

}
