
package bh11_homework_03;

import java.util.Scanner;


public class BH11_Homework3_09 {
    public static void main(String[] args) {
        /*
        A felhasználótól kérj be 2 egész számot 10 és 500 között. 
        Határozd meg a két pozitív egész szám legnagyobb közös osztóját!
        */
        
        Scanner scanner = new Scanner(System.in);
        int num1 = 0;
        int num2 = 0;
        boolean asking = true;
        
        do {
            System.out.println("Adj meg egy egész számot 10 és 500 között!");
            if (scanner.hasNextInt()) {
                num1 = scanner.nextInt();
                if (num1>=10 && num1<=500) { //megengedi az egyenlőséget
                    asking = false;
                }
            }else {
            scanner.next();
            }
        } while (asking);
        
        asking = true;
        
        do {
            System.out.println("Adj meg még egy egész számot 10 és 500 között!");
            if (scanner.hasNextInt()) {
                num2 = scanner.nextInt();
                if (num2>=10 && num2<=500) { //megengedi az egyenlőséget
                    asking = false;
                }
            }else {
            scanner.next();
            }
        } while (asking);
        
        int min = Math.min(num1, num2);
        int max = Math.max(num1, num2);
    
        for (int modulo = max % min; modulo > 0;) {
            max = min;
            min = modulo;  
            modulo = max % min;
        }
        System.out.println("A legnagyobb közös osztó: "+ min); 
        
    }
    
}
