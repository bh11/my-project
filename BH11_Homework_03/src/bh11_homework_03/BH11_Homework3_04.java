package bh11_homework_03;

import java.util.Scanner;

public class BH11_Homework3_04 {

    public static void main(String[] args) {

        /*
        4. Kérj be 2 számot a felhasználótól. 
        A kisebbtől a nagyobbig írd ki a 11-re végződő számokat!
         */
        Scanner scanner = new Scanner(System.in);

        int num1 = 0;
        int num2 = 0;
        boolean asking = true;

        do {
            System.out.println("Kérlek írj be egy pozitív egész számot!");
            if (scanner.hasNextInt()) {
                num1 = scanner.nextInt();
                if (num1 > 0) {
                    asking = false;
                }
            } else {
                scanner.next();
            }
        } while (asking);

        asking = true;

        do {
            System.out.println("Kérlek írj be még egy pozitív egész számot!");
            if (scanner.hasNextInt()) {
                num2 = scanner.nextInt();
                if (num2 > 0) {
                    asking = false;
                }
            } else {
                scanner.next();
            }
        } while (asking);

        int min = Math.min(num1, num2);
        int max = Math.max(num1, num2);

        while (min <= max) {

            String temp = Integer.toString(min);
            String isEleven = "11";
            String substring = temp.length() > 2 ? temp.substring(temp.length() - 2) : temp;
            if (substring.equals(isEleven)) {
                System.out.println(min);
            }
            min++;
        }
        int min2 = Math.min(num1, num2);
        int max2 = Math.max(num1, num2);

       
        System.out.println("Ugyanez sokkal könnyebben:");

        
        while (min2 <= max2) {

            if (min2 == 11) {
                System.out.println(min2);
            } else if (min2 % 100 == 11) {
                System.out.println(min2);
            }
            min2++;
        }

    }

}
