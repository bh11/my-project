package bh11_designpatterns;

import bh11_designpatterns.dp.creation.Car;
import bh11_designpatterns.dp.creation.Car2;
import bh11_designpatterns.dp.creation.MySingleton;
import bh11_designpatterns.dp.creation.factory.AbstractAnimal;
import bh11_designpatterns.dp.creation.factory.AnimalFactory;

public class App {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        Car c = new Car.CarBuilder("trabi", "feher")
                            .setAbs(true)
                            .setMaxSpeed(221)
                            .build();
        
        Car2 c2 = new Car2("trabi", "feher")
                        .setAbs(true)
                        .setMaxSpeed(221);
        
        AbstractAnimal ab = AnimalFactory.create("programmer");
        ab.sayHello();
    } 
    
    public static void test() {
        MySingleton ms = MySingleton.getInstance();
    }
}
