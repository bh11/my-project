package bh11_designpatterns.dp.creation;

public class MySingleton {

    private static MySingleton instance;
    
    private MySingleton() {//letiltom a peldanyositast kivulrol

    }
    
    public static MySingleton getInstance() {//nem threadsafe
        if(instance == null) {
            instance = new MySingleton();
        }
        
        return instance;
    }
    
    public boolean validateAge(int age) {
        if (age < 14) {
            return false;
        }
        return true;
    }
}
