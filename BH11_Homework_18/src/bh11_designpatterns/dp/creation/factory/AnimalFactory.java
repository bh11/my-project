package bh11_designpatterns.dp.creation.factory;

public class AnimalFactory {
    public static AbstractAnimal create(String type) {
        if("dog".equalsIgnoreCase(type)) {
            return new Dog();
        } else if ("cat".equalsIgnoreCase(type)) {
            return new Cat();
        } else if ("programmer".equalsIgnoreCase(type)) {
            return new Programmer();
        }

        return null;
    }
}
