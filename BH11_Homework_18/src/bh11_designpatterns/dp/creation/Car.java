package bh11_designpatterns.dp.creation;

public class Car {
    private String model;
    private String color;
    private boolean abs;
    private int maxSpeed;
    private boolean spoiler;
    
    public static class CarBuilder {
        private String model;
        private String color;
        private boolean abs;
        private int maxSpeed;
        private boolean spoiler;
        
        public CarBuilder(String model, String color) {
            this.model = model;
            this.color = color;
        }

        public CarBuilder setAbs(boolean abs) {
            this.abs = abs;
            return this;
        }

        public CarBuilder setMaxSpeed(int maxSpeed) {
            this.maxSpeed = maxSpeed;
            return this;
        }

        public CarBuilder setSpoiler(boolean spoiler) {
            this.spoiler = spoiler;
            return this;
        }
        
        public Car build() {
            return new Car(this);
        }
    }
    
    public Car(CarBuilder cb) {
        this.model = cb.model;
        this.color = cb.color;
        this.abs = cb.abs;
        this.spoiler = cb.spoiler;
        this.maxSpeed = cb.maxSpeed;
    }
}
