package bh11_homework_13;

public class BH11_Homework_13 {

    public static void main(String[] args) {
        /*
        String str = " Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
         - A fenti string hány mondatból áll? 
         - A fenti stringben hányszor szerepel az "it" részlet
         - írjuk ki a fenti szövegben az első és az utolsó "it" közötti szövegrészletet
         - írjunk ki minden második szót
         - cseréljük le az 'a' betűket 'A' betükre
         - írjuk ki fordítva (StringBuilder metódus segítségével)
         */
        String originalSentence = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed "
                + "do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim "
                + "ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex "
                + "ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit "
                + "esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non "
                + "proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

        String[] stringArray = originalSentence.split("\\.");
        int numberOfSentences = stringArray.length;
        System.out.println("A mondatok száma: " + numberOfSentences);

        int counter = 0;
        for (int i = 0; i < originalSentence.length(); i++) {
            int index = originalSentence.indexOf("it", i);
            if (index >= 0) {
                counter++;
            }
        }
        System.out.printf("Az it részlet %d alkalommal szerepel \n", counter);

        int firstIndexOfIt = originalSentence.indexOf("it") - 2; //-2, mivel az "it" két karakter
        int lastIndexOfIt = originalSentence.lastIndexOf("it");
        String stringBetweenIts = originalSentence.substring(firstIndexOfIt, lastIndexOfIt);
        System.out.println(stringBetweenIts);

        String[] secondWords = originalSentence.split("\\s");

        for (int i = 0; i < secondWords.length; i = i + 2) {
            System.out.println(secondWords[i]);
        }

        String upperCaseA = originalSentence.replace("a", "A");
        System.out.println(upperCaseA);

        StringBuilder backward = new StringBuilder(originalSentence);
        System.out.println(backward.reverse());
        
    }
}
