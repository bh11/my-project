package bh11_class005;

import java.util.Scanner;

public class BH11_Class005_MiniGame {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int row = 0;
        int column = 0;
        char gift = 'A';
        int numberOfGifts = 9;
        char bomb = 'B';
        int numberOfBombs = 15;
        boolean ok = false;

        // PÁLYA MÉRETÉNEK MEGADÁSA
        System.out.println("Add meg a pálya méretét! (pl. 20*50)");
        do {
            if (scanner.hasNextInt()) {
                row = scanner.nextInt();
                column = scanner.nextInt();
                if (row >= 20 && column >= 20) {
                    ok = true;
                }
            } else {
                scanner.next();
            }
        } while (!ok);

        // PÁLYA FELTÖLTÉSE SPACEKKEL
        char[][] field = new char[row][column];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                field[i][j] = ' ';
            }
        }

        // AJÁNDÉK KIRAJZOLÁSA
        for (int i = 0; i < numberOfGifts; i++) {
            int x = (int) (Math.random() * row);
            int y = (int) (Math.random() * column);

            if (field[x][y] != gift) {
                field[x][y] = gift;
            } else {
                i--;
            }

        }
        // BOMBA KIRAJZOLÁSA
        for (int i = 0; i < numberOfBombs; i++) {
            int x = (int) (Math.random() * row);
            int y = (int) (Math.random() * column);

            if (field[x][y] != bomb && field[x][y] != gift) {
                field[x][y] = bomb;
            } else {
                i--;
            }
// PÁLYA KIRAJZOLÁSA BOMBÁKKAL ÉS AJÁNDÉKOKKAL
        }
        System.out.println("------------------------------------------");
        for (int i = 0; i < field.length; i++) {
            System.out.print("|");
            for (int j = 0; j < field[i].length; j++) {
                System.out.print(" " + field[i][j]);
            }
            System.out.print("|");
            System.out.println();
        }
        System.out.println("------------------------------------------");
    }

}
