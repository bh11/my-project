package bh11_class005;

public class BH11_Class005_Methods {

    static boolean sendMail(String to, String text) {
        if (to.contains("@")) {
            System.out.println("E-mail has been sent!");
            return true;
        }
        System.out.println("Something went wrong...");
        return false;
    }

    static void printName() {
        System.out.println("Zsuga T. Gabriella");
        return; // Ez akkor is itt van, ha nem írjuk ki, ez az utolsó utasítás.  Nincs true vagy false értéke.
    }

    static boolean isEven(int number) {
        if (number % 2 == 0) {
            return true;            // return number%2==0; Ez az egysoros megoldás
        }
        return false;
    }

    //Pareméterül vesz két egész számot és visszaadja őket összefűzve
    static String concat(int a, int b) {
//        return a + " " + b;
//        return "" + a + b;
//        return String.valueOf(a)+String.valueOf(b);
          return String.format("%d%d",a, b);
    }

    public static void main(String[] args) {

        sendMail("gabriella.zsuga@gmail.com", "blabla");
        sendMail("gabriella.zsuga.gmail.com", "blabla");

        boolean success = sendMail("gabreilla@", "blabla");
        System.out.println(success);

        printName();
        int a = 12;
        System.out.println(isEven(a));
        int b =30;
        System.out.println(concat(a, b));
        System.out.println(concat(20, 321));
        String s = concat(a,b);
        System.out.println(s);
    }

}
