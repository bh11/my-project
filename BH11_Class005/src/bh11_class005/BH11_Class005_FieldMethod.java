package bh11_class005;

public class BH11_Class005_FieldMethod {

    static void printField(int[][] field) {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {

                System.out.print(" " + field[i][j]);
            }
            System.out.println();
        }
    }

    static int generateRandomNumber(int to) {
        int randomNumber = (int) (Math.random() * to);
        return randomNumber;
        // vagy return (int) (Math.random() * to);
    }

    static void putGifts(int[][] field, int icon, int count) {
        for (int i = 0; i < count; i++) {
            int x = generateRandomNumber(field.length);
            int y = generateRandomNumber(field[i].length);

            if (field[x][y] != icon) {
                field[x][y] = icon;
            } else {
                i--;
            }

        }
    }

    public static void main(String[] args) {

        /*
        Egy 8*9-es pályára generáljunk 7 ajándékot.
        Egy helyre csak egy ajándék kerülhet.
        Rajzoljuk ki a pályát!
         */
        int row = 8;
        int column = 9;
        int gift = 1;
        int numberOfGifts = 7;
        int[][] field = new int[row][column];

        putGifts(field, gift, numberOfGifts);
        printField(field);
    }

}
