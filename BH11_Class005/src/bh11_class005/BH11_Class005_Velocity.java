package bh11_class005;

import java.util.Scanner;

public class BH11_Class005_Velocity {

    public static void main(String[] args) {

        /*
        Az m7-es autópályán trafipaxot szerelnek fel. 
        A mérés adatai: óra perc sebesség (pl. 12 45 198)
        Több napi mérést tudunk megnézni. 
        Készíts egy táblázatot, hogy melyik órában mennyivel ment a leggyorsabb autó
        Ha egy adott órában nem volt gyorshajtás, az maradjon ki!
        A kimenet: 14:00-14:49 --> 145km/h
        Ha 0 0 0-t kap értékül, lépjen ki.
         */
 /*
        Elég egy 24 elemű tömbb, ami reprezentálja a 24 órát.
        Csak a legnagyobb sebességet kell eltárolnunk. 
        1D 24 elemű tömb, amibe a sebességet tároljuk. 
        Ha az új sebesség nagyobb, mint a régi, akkor írja felül.
         */
        Scanner scanner = new Scanner(System.in);
        int hour = 0;
        int minute = 0;
        int speed = 0;
        boolean zero = true;
        int[] array = new int[24];
        int minSpeed = Integer.MIN_VALUE;

        do {

            hour = scanner.nextInt();
            minute = scanner.nextInt();
            speed = scanner.nextInt();

            if (hour == 0 && minute == 0 && speed == 0) {
                zero = false;
            }

            if (speed > 130) {
                if (speed > minSpeed) {
                    array[hour] = speed;
                }
            }
        } while (zero);

        System.out.println("Melyik óra gyorshajtását szeretnéd látni?");
        hour = scanner.nextInt();
        System.out.println("A legnagyobb mért gyorshajtás: " + array[hour] + "km/h");
    }

}
