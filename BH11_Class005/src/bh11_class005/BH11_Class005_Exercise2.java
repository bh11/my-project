package bh11_class005;

import java.util.Scanner;

public class BH11_Class005_Exercise2 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String sentence;
        String longestSentence = "";
        int counter = 1;
        int isTheLongest = 0;
        boolean ok = false;
        do {
            sentence = scanner.nextLine();
            if (sentence.equals("exit")) {
                ok = true;
            }
            if (sentence.length() > longestSentence.length()) {
                longestSentence = sentence;
                isTheLongest = counter;
            }
            counter++;

        } while (!ok);

        System.out.println("A leghosszabb mondat: " + longestSentence);
        System.out.println(isTheLongest + ". bevitel");
    }

}
