package bh11_class005;

import java.util.Scanner;

public class BH11_Class005_Exercise1 {

    public static void main(String[] args) {

        /*
        Kérjünk be a felhasználótól 10db pozitív egész számot!
        Add meg, hány 3-mal osztható van közöttük!
        Add meg a számok átlagát!
         */
        Scanner scanner = new Scanner(System.in);
        int number;
        int counter = 0;
        int divideByTrhee = 0;
        int sum = 0;
        int[] array = new int[10];

        do {
            if (scanner.hasNextInt()) {
                number = scanner.nextInt();
                if (number > 0) {
                    array[counter] = number;
                    counter++;
                    sum += number;
                    if (number % 3 == 0) {
                        divideByTrhee++;
                    }
                }
            } else {
                scanner.next();
            }
        } while (counter < 10);

        System.out.print("A tömb elemei: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(" " + array[i]);
        }
        System.out.println();
        System.out.println(divideByTrhee + " szám osztható 3-mal");
        System.out.println("A számok átlaga: " + (double) sum / array.length);

        //////////////// MÁSIK MEGOLDÁS ////////////////////////
        int number2;
        int counter2 = 0;
        int divideByTrhee2 = 0;
        int sum2 = 0;
        do {
            if (scanner.hasNextInt()) {
                number2 = scanner.nextInt();
                if (number2 > 0) {
                    counter2++;
                    sum2 += number2;
                    if (number2 % 3 == 0) {
                        divideByTrhee2++;
                    }
                }
            } else {
                scanner.next();
            }
        } while (counter2 < 10);

        System.out.println(divideByTrhee2 + " szám osztható 3-mal");
        System.out.println("A számok átlaga: " + (double) sum2 / counter2);
    }

}
