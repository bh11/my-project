
package bh11_class005;


public class BH11_Class005_Field {
    public static void main(String[] args) {
        
        /*
        Egy 8*9-es pályára generáljunk 7 ajándékot.
        Egy helyre csak egy ajándék kerülhet.
        Rajzoljuk ki a pályát!
        */
        
        int row = 8;
        int column = 9;
        int gift = 1;
        int numberOfGifts = 7;
        int[][] field = new int[row][column];

        for (int i = 0; i < numberOfGifts; i++) {
            int x = (int) (Math.random() * row);
            int y = (int) (Math.random() * column);

            if (field[x][y] != gift) {
                field[x][y] = gift;
            } else {
                i--;
            }
        }

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {

                System.out.print(" " + field[i][j]);
            }
            System.out.println();
        }
    }
    
}
