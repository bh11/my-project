
package bh11_class010_books;

import sun.security.x509.AuthorityInfoAccessExtension;


public class App {


    public static void main(String[] args) {

        Authors author1 = new Authors("Tolkien");
        Authors author2 = new Authors("J.R.R. Martin");
        
        Books book1 = new Books("Lord of the Rings");
        Books book2 = new Books("Game Of Thrones");
        Books book3 = new Books("Hobbit");

       
        author1.addBook(book1);
        author1.addBook(book3);
        author2.addBook(book2);
        author1.listOfBooks();
        author2.listOfBooks();
        
    }
    
}
