
package hu.braininghub.mysql.jdbc;

import java.util.Scanner;


public class ConsoleReader {
 private static final String STOP = "Exit";   
    
    HrDao dao = new HrDao();
    
    public void start(){            
    try(Scanner sc = new Scanner(System.in)){
    String line;
 do {
     line = sc.nextLine();
     switch (line){
         case "1": dao.findEmployeeWithMaxSalary();break;
         case "2": dao.departmentWithTheMostEmployee(); break;
         case "3": dao.salaryBiggerThanAvg(); break;
         case "4": dao.jobHistoryFrom1990(); break;
         case "5": dao.jobTitleContainsClerk(); break;
         case "6": 
             System.out.println("Give me an ID:");
             String id = sc.nextLine();
             dao.getEmployeeByID(id);
             break;
         case "7": 
             System.out.println("Give me a first name:");
             String name = sc.nextLine();
             dao.findEmpByFirstName(name); break;
     } 
        } while (!STOP.equalsIgnoreCase(line));
    }
    }
}
