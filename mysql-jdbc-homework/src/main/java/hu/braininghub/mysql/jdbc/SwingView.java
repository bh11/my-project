
package hu.braininghub.mysql.jdbc;

import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class SwingView extends JFrame{
    
    private HrDao dao = new HrDao();
    
    private final JButton jb1 = new JButton("Employee with the maximum salary");
    private final JButton jb2 = new JButton("Department with the most employee");
    private final JButton jb3 = new JButton("Employees with salary better that avg");
    private final JButton jb4 = new JButton("Job history from 1990.01.01.");
    private final JButton jb5 = new JButton("Job titles contains clerk ");
    private final JButton jb6 = new JButton("Find employee by ID");
    private final JButton jb7 = new JButton("Find employee by first name");
    private final JTextArea jTextArea = new JTextArea(20,20);
    private final JTextField inputText = new JTextField(50);
    private final JTextField outputText = new JTextField(50);
    private final JTextField inputText2 = new JTextField(50);
    private final JTextField outputText2 = new JTextField(50);
            
     public void start() {
        buildView();
    }

    public void buildView() {
        add(buildPanel1(), BorderLayout.NORTH);
        add(buildPanel2(), BorderLayout.CENTER);
        add(buildPanel3(), BorderLayout.SOUTH);
        
        jb1.addActionListener(l->jTextArea.setText(dao.findEmployeeWithMaxSalary().stream().collect(Collectors.joining("\n"))));
        jb2.addActionListener(l->jTextArea.setText(dao.departmentWithTheMostEmployee().stream().collect(Collectors.joining("\n"))));
        jb3.addActionListener(l->jTextArea.setText(dao.salaryBiggerThanAvg().stream().collect(Collectors.joining("\n"))));
        jb4.addActionListener(l->jTextArea.setText(dao.jobHistoryFrom1990().stream().collect(Collectors.joining("\n"))));
        jb5.addActionListener(l->jTextArea.setText(dao.jobTitleContainsClerk().stream().collect(Collectors.joining("\n"))));
        
        jb6.addActionListener(l->outputText.setText(dao.getEmployeeByID(inputText.getText()).stream().collect(Collectors.joining("\n"))));
        jb7.addActionListener(l->outputText2.setText(dao.findEmpByFirstName(inputText2.getText()).stream().collect(Collectors.joining(", "))));
        
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    private JPanel buildPanel1() {
        JPanel panel1 = new JPanel();
        GridLayout gbl = new GridLayout();
        panel1.setLayout(gbl);
        panel1.add(jb1);
        panel1.add(jb2);
        panel1.add(jb3);
        panel1.add(jb4);
        panel1.add(jb5);
        JScrollPane scroll = new JScrollPane(jTextArea);
        panel1.add(scroll);
        

        return panel1;
    }
    
    
    private JPanel buildPanel2(){
    JPanel panel2 = new JPanel();
    panel2.add(jb6);
    panel2.add(inputText);
    panel2.add(outputText);
    return panel2;
    }
    
    private JPanel buildPanel3(){
    JPanel panel3 = new JPanel();
    panel3.add(jb7);
    panel3.add(inputText2);
    panel3.add(outputText2);
    return panel3;
    }  
}
