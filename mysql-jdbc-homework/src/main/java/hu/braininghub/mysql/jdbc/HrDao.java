package hu.braininghub.mysql.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HrDao {

    private static final String URL = "jdbc:mysql://localhost:3306/hr?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PW = "Gaga1304";

//    public HrDao(){
//       try {
//           Class.forName("com.mysql.jdbc.Driver");
//       } catch (ClassNotFoundException ex) {
//           Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
//       }
//    }
    public List<String> findEmployeeWithMaxSalary() {
        String sql = "select first_name, last_name from employees having max(salary) limit 1";
        List<String> salaryList = new ArrayList<>();
        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                salaryList.add(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
            return salaryList;
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return salaryList;
    }

    public List<String> departmentWithTheMostEmployee() {
        List<String> departmentList = new ArrayList<>();
        String sql = "SELECT department_name, COUNT(employee_id) AS emp FROM departments"
                + " INNER JOIN employees on employees.department_id=departments.department_id "
                + "GROUP BY department_name ORDER BY emp DESC LIMIT 1;";
        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                departmentList.add(rs.getString("department_name"));
            }
            return departmentList;
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return departmentList;
    }

    public List<String> salaryBiggerThanAvg() {
        List<String> salaryBiggerThanAvg = new ArrayList<>();
        String sql = "select first_name, last_name from employees where (select avg(salary) from employees)<salary";
        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                salaryBiggerThanAvg.add(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
            return salaryBiggerThanAvg;
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return salaryBiggerThanAvg;
    }

    public List<String> jobHistoryFrom1990() {
        List<String> jobList = new ArrayList<>();
        String sql = "select first_name, last_name from employees left join job_history on start_date>\"1990-01-01\"";
        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                jobList.add(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
            return jobList;
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jobList;
    }

    public List<String> jobTitleContainsClerk() {
        List<String> containsClerk = new ArrayList<>();
        String sql = "select distinct job_title from jobs where job_title like \"%clerk%\" order by job_title";
        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement();) {
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                containsClerk.add(rs.getString("job_title"));
            }
            return containsClerk;
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return containsClerk;
    }

    public List<String> getEmployeeByID(String id) {
        List<String> empByIDList = new ArrayList<>();
        String sql = "select * from employees e where e.employee_id = ?";
        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                PreparedStatement pstm = conn.prepareStatement(sql);) {
            pstm.setInt(1, Integer.parseInt(id));
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                empByIDList.add(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
            return empByIDList;
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return empByIDList;
    }

    public List<String> findEmpByFirstName(String name) {
        List<String> empByName = new ArrayList<>();
        String sql = "select * from employees e where e.first_name = ?";
        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                PreparedStatement pstm = conn.prepareStatement(sql);) {
            pstm.setString(1, name);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                empByName.add(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
            return empByName;
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return empByName;
    }

}
