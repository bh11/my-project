package bh11_homework_15;

public class CreditCard extends DebitCard {

    /*
    Olyan Bankkártya, amelyhez Hitel tartozik. A hitelkártyához legenerálódó hitelösszege alapértelmezésként 0 Ft, így törleszteni se kell.
    Ám ha a hitelkártya egyenlegét 0 forint alá próbálják csökkenteni (vásárlással, készpénzfelvétellel, stb.), akkor a kártyaegyenleg 0 Ft maradjon,ám a kártyához tartozó hitel összege növekedjen a szükséges értékkel. 
    Pl. 1500 Ft-os egyenleg és 2000 Ft-os vásárlás esetén a hitel összege 500 Ft-talnövekedjen.
    A hitelkártya-hitel törlesztési ideje a lakáshitellel szemben 1 hónap. 
    Így az adott hónapban felvett hitel a hónap végén (tehát havi záráskor) kamatostul visszafizetendő. 
    A hitelkártya-hitel kamata szintén 6%.
     */
    private CreditCardCredit loan;
    private int creditCardBalance;
    public static final double INTEREST = 0.06;
    public static final int MATURITY = 1;

    public int getCreditCardBalance() {
        return creditCardBalance;
    }

    public void setCreditCardBalance(int creditCardBalance) {
        this.creditCardBalance = creditCardBalance;
    }

    public CreditCardCredit getLoan() {
        return loan;
    }

    public void setLoan(CreditCardCredit loan) {
        this.loan = loan;
    }


}
