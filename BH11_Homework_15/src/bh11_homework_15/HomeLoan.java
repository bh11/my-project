package bh11_homework_15;

public class HomeLoan extends BankAccount{


    /*
    Bankszámlához kapcsolódik, amelyről a törlesztőrészletek havonta levonásra kerülnek.
    Hitelösszege, havi törlesztőrészlete és kamata van, a kamat a teljes hitelösszeg 6%-a
    Minden lakáshitel futamideje 3 hónap, havi törlesztőrészlete az eredeti hitelösszeg egyharmada + a kamat egyharmada
     */
    
    private int amountOfCredit;
    private double repayment;
    public static final double INTEREST = 0.06;
    public static final int MATURITY = 3;
    
    public HomeLoan(int amountOfCredit){
    this.amountOfCredit = amountOfCredit;
    }
    

    public int getAmountOfCredit() {
        return amountOfCredit;
    }

    public double getRepayment() {
        repayment = amountOfCredit/MATURITY + amountOfCredit*INTEREST/MATURITY;
        return repayment;
    }

    public void setAmountOfCredit(int amountOfCredit) {
        this.amountOfCredit = amountOfCredit;
    }
    
    
}
