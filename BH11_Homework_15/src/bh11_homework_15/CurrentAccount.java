package bh11_homework_15;

public class CurrentAccount extends BankAccount{


    /*
    Havi számlavezetési díja 1500 Ft, de ha a számlán az adott hónap végén több mint 250.000 Ft-nyi összeg van, kedvezményesen csak 500 Ft
    Átutalási díj: az utalt összeg 5%-a, a küldő fél számlájáról kerüllevonásra.
     */

    public static final int ACCOUNT_FEE = 1500;
    public static final int MIN_MONEY_TO_GET_DISCOUNT = 250000;
    public static final int REDUCED_ACCOUNT_FEE= 500;
    public static final double TRANSFER_FEE = 0.05;

    @Override
    public String toString() {
        return super.toString()+"CurrentAccount{" + '}';
    }



}
