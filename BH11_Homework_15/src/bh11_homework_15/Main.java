package bh11_homework_15;

public class Main {

    public static void main(String[] args) {
        /* Hint:
        Bankszámla: absztrakt osztály (Lakossági és diákszámla alosztály)
        A felhasználó is egy osztály
        A bankszámla felhasználóhoz kötött
        Hitel absztrakt osztály, hozzá a két sima osztály
        Collection: egy felhasználóhoz lehet több kártya és egy bankszámlához tartozhat többb hitel
         */

        Person Gyula = new Person(50, 200000);
        Person Kata = new Person(16, 40000);

        System.out.println("Számla nyitása:");
        Gyula.openAccount();
        Gyula.openAccount();
        Kata.openAccount();

        System.out.println("Gyula: " + Gyula);
        System.out.println("Kata: " + Kata);

        System.out.println();

        System.out.println("Pénz befizetése:");
        Gyula.putMoneyOnTheAccount(Gyula.chooseAccount(1), 100000);
        Gyula.putMoneyOnTheAccount(Gyula.chooseAccount(2), 50000);
        Kata.putMoneyOnTheAccount(Kata.chooseAccount(1), 30000); //itt a túlindexelést még javítani kell
        System.out.println("Gyula: " + Gyula);
        System.out.println("Kata: " + Kata);

        System.out.println();

        System.out.println("Bankkártya igénylése:");
        Gyula.openDebitCard(new DebitCard(), Gyula.chooseAccount(2));
        Kata.openDebitCard(new DebitCard(), Kata.chooseAccount(1));

        System.out.println("Gyula: " + Gyula);
        System.out.println("Kata: " + Kata);

        System.out.println();

        System.out.println("Készpénzfelvétel:");
        Gyula.withdrawCashFromTheBank(Gyula.chooseAccount(1), 10000);
        Kata.withdrawCashFromAnATM(Kata.chooseAccount(1), 10000);

        System.out.println("Gyula: " + Gyula);
        System.out.println("Kata: " + Kata);

        System.out.println();

        System.out.println("Kártyás vásárlás:");
        Gyula.payByDebitCard(Gyula.chooseAccount(2), 20000);
        Kata.payByDebitCard(Kata.chooseAccount(1), 10000);

        System.out.println("Gyula: " + Gyula);
        System.out.println("Kata: " + Kata);

        System.out.println();

        System.out.println("Átutalás:");
        Gyula.transferMoney(Gyula.chooseAccount(1), Kata.chooseAccount(1), 30000);

        System.out.println("Gyula: " + Gyula);
        System.out.println("Kata: " + Kata);

        System.out.println();

        System.out.println("Hitelfelvétel:");
        Gyula.addHomeLoanToAccount(Gyula.chooseAccount(1), new HomeLoan(20000));
        System.out.println("Gyula: " + Gyula);

        System.out.println();

        System.out.println("Hitelkártya igénylés:");
        Gyula.openCreditCard(new CreditCard()); //hitelkártya hitel összekötésével hiányos
        
        System.out.println();

        System.out.println("Havi zárás:"); //hitelkártya hitellel hiányos
        Gyula.monthlyClose(Gyula.chooseAccount(1));
        Gyula.monthlyClose(Gyula.chooseAccount(2));
        Kata.monthlyClose(Kata.chooseAccount(1));

        System.out.println("Gyula: " + Gyula);
        System.out.println("Kata: " + Kata);
    }

}
