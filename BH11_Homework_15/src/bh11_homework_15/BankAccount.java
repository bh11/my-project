package bh11_homework_15;

public abstract class BankAccount {

    /*
    Felhasználóhoz kapcsolódik, számlaszámmal rendelkezik
    Számlanyitási díj: 500 Ft
    Pénz befizetése ingyenes, a készpénzfelvétel díja a kártyás kézpénzfelvételével megegyezik
     */
    private int accountNumber;
    private int amountOfMoney;
    public static final int FEE_FOR_OPENING = 500;
    public static final double VAT = 0.27;
    public static final double WITHDRAW_CASH_FEE = 0.1;
    public static final int MAX_AMOUNT_OF_WITHRAW_FEE = 1000;

    private DebitCard debitCard;
    private CreditCard creditCard;
    private HomeLoan homeLoan;

    public void debitCardFee(DebitCard debitCard) {
        amountOfMoney -= DebitCard.FEE_FOR_OPENING;
    }
     public void creditCardFee(CreditCard creditCard) {
        amountOfMoney -= DebitCard.FEE_FOR_OPENING;
    }

    public void cashWithdraw(int money) {
        if (notHaveEnoughMoneyToWithdraw(money)) {
            System.out.println("A számlán nincs elég pénz!");
        }else if (withdrawFeeIsNotMax(money)) {
            amountOfMoney -= (int) money + (money * WITHDRAW_CASH_FEE);
        } else {
            amountOfMoney -= (int) money - MAX_AMOUNT_OF_WITHRAW_FEE;
        }
    }

    private boolean notHaveEnoughMoneyToWithdraw(int money) {
        return withdrawFeeIsNotMax(money) && (money + money * WITHDRAW_CASH_FEE) > amountOfMoney
                || !withdrawFeeIsNotMax(money) && money - MAX_AMOUNT_OF_WITHRAW_FEE > amountOfMoney;
    }

    private boolean withdrawFeeIsNotMax(int money) {
        return money * WITHDRAW_CASH_FEE <= MAX_AMOUNT_OF_WITHRAW_FEE;
    }

    public void cashWithdrawWithDebitCard(int money) {
        amountOfMoney -= debitCard.cashWithdraw(money);
    }

    public double monthlyHomeLoanRepayment(){
        return homeLoan.getRepayment();
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public int getAmountOfMoney() {
        return amountOfMoney;
    }

    public DebitCard getDebitCard() {
        return debitCard;
    }

    public static double getVAT() {
        return VAT;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }
    public HomeLoan getHomeLoan() {
        return homeLoan;
    }
    
    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setAmountOfMoney(int amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    public void setDebitCard(DebitCard debitCard) {
        this.debitCard = debitCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public void setHomeLoan(HomeLoan homeLoan) {
        this.homeLoan = homeLoan;
    }


    

    @Override
    public String toString() {
        return "BankAccount{" + "accountNumber=" + accountNumber + ", amountOfMoney=" + amountOfMoney + '}';
    }

}
