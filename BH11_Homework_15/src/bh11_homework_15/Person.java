package bh11_homework_15;

import java.util.List;
import java.util.ArrayList;

public class Person {
    
    private int age;
    private int cash;
    private BankAccount bankAccount;
    private List<BankAccount> bankAccountList = new ArrayList<>();
    private List<DebitCard> debitCardList = new ArrayList<>();
    private List<CreditCard> creditCardList = new ArrayList<>();
    
    public Person(int age, int cash) {
        this.age = age;
        this.cash = cash;
    }
    
    public void openAnAccount(BankAccount bankAccount) {
        if (notEnoughMoneyToOpenAccount()) {
            System.out.println("Nincs elég pénz a kártyanyitáshoz!");
        } else {
            if (age >= 18 && bankAccount instanceof StudentAccount) {
                System.out.println("A bankszámlanyitás nem lehetséges!");
            } else {
                bankAccountList.add(bankAccount);
                cash -= BankAccount.FEE_FOR_OPENING;
            }
        }
    }
    
    public void openAccount() {
        if (notEnoughMoneyToOpenAccount()) {
            System.out.println("Nincs elég pénz a kártyanyitáshoz!");
        } else {
            if (age <= 18) {
                bankAccount = new StudentAccount();
            } else {
                bankAccount = new CurrentAccount();
            }
            bankAccountList.add(bankAccount);
            cash -= BankAccount.FEE_FOR_OPENING;
        }
        
    }
    
    public void transferMoney(BankAccount from, BankAccount to, int money) {
        double transferFee = 0;
        if (from instanceof CurrentAccount) {
            transferFee = money * CurrentAccount.TRANSFER_FEE;
        }
        if (from.getAmountOfMoney() - (money + transferFee) > 0) {
            from.setAmountOfMoney(from.getAmountOfMoney() - (int) (money + transferFee));
            to.setAmountOfMoney(to.getAmountOfMoney() + money);
        } else {
            System.out.println("A tranzakció végrehajtásához nem áll rendelkezésre elég fedezet!");
        }
    }
    
    public void payByDebitCard(BankAccount account, int money) {
        if (account.getDebitCard() != null) {
            if (account.getAmountOfMoney() < money) {
                System.out.println("Nem áll rendelkezésre megfelelő fedezet!");
            } else {
                account.setAmountOfMoney(account.getAmountOfMoney() - money);
            }
        } else {
            System.out.println("A számlához nem tartozik bankkártya!");
        }
    }
    
//    public void payByCreditCard(BankAccount account, int money) {
//        if (account.getCreditCard() != null) {
//            if (account.getAmountOfMoney() < money) {
//                System.out.println("Nem áll rendelkezésre megfelelő fedezet!");
//            } else {
//                account.setAmountOfMoney(account.getAmountOfMoney() - money);
//            }
//        } else {
//            System.out.println("A számlához nem tartozik bankkártya!");
//        }
//    }
    
//    public void payByCreditCard(CreditCard creditCard, int loan){
//        
//    
//    }
//    
    private boolean notEnoughMoneyToOpenAccount() {
        return cash < 500;
    }
    
    public void putMoneyOnTheAccount(BankAccount bankAccount, int money) {
        if (bankAccount != null) {
            if (cash > money) {
                bankAccount.setAmountOfMoney(bankAccount.getAmountOfMoney() + money);
                cash -= money;
            } else {
                System.out.println("Nincs elég pénz a kártya feltöltéséhez!");
            }
        } else {
            System.out.println("Nincs a kérésnek megfelelő bankszámla!");
        }
        
    }
    
    public void openDebitCard(DebitCard debitCard, BankAccount bankAccount) {
        if (bankAccount.getAmountOfMoney() > DebitCard.FEE_FOR_OPENING) {
            bankAccount.setDebitCard(debitCard);
            bankAccount.debitCardFee(debitCard);
            debitCardList.add(debitCard);
        } else {
            System.out.println("Nincs elegendő fedezet bankkártya igényléséhez!");
        }
    }
    
    public void openCreditCard(CreditCard creditCard) {
        creditCard.setLoan(new CreditCardCredit(0));
        creditCardList.add(creditCard);
    }
    
    public void addHomeLoanToAccount(BankAccount bankAccount, HomeLoan homeLoan) {
        bankAccount.setHomeLoan(homeLoan);
    }
    
    public BankAccount chooseAccount(int number) {
        BankAccount account;
        if (number > bankAccountList.size() || number <= 0) {
            System.out.println("Nincs a kérésnek megfelelő bankszámla!");
            return null;
        } else {
            return account = bankAccountList.get(number - 1);
        }
    }
    
    public CreditCard chooseCreditCard(int number) {
        CreditCard card;
        if (number > bankAccountList.size() || number <= 0) {
            System.out.println("Nincs a kérésnek megfelelő bankszámla!");
            return null;
        } else {
            return card = creditCardList.get(number - 1);
        }
    }

    public void withdrawCashFromTheBank(BankAccount bankAccount, int money) {
        bankAccount.cashWithdraw(money);
        cash += money;
    }
    
    public void withdrawCashFromAnATM(BankAccount bankAccount, int money) {
        if (bankAccount.getDebitCard() != null) {
            bankAccount.cashWithdrawWithDebitCard(money);
            cash += money;
        } else {
            System.out.println("A számlához nem tartozik bankkártya!");
        }
    }
    
    public void monthlyClose(BankAccount bankAccount){
        monthlyFees(bankAccount);
        monthlyHomeLoanFees(bankAccount);
    } 
    
    private void monthlyHomeLoanFees(BankAccount bankAccount){
            if (bankAccount.getHomeLoan()!=null) {
            bankAccount.setAmountOfMoney(bankAccount.getAmountOfMoney()- (int) bankAccount.monthlyHomeLoanRepayment());
        }
    }
    
    private void monthlyFees(BankAccount bankAccount){
    if (bankAccount instanceof StudentAccount) {
            bankAccount.setAmountOfMoney(studentAccountFee());
        }else{
            if (bankAccount.getAmountOfMoney()> CurrentAccount.MIN_MONEY_TO_GET_DISCOUNT) {
                bankAccount.setAmountOfMoney(currentAccountFee(CurrentAccount.REDUCED_ACCOUNT_FEE));
            }else{
            bankAccount.setAmountOfMoney(currentAccountFee(CurrentAccount.ACCOUNT_FEE));
            }
        }
    }
    
    private int studentAccountFee(){
    return bankAccount.getAmountOfMoney()-StudentAccount.ACCOUNT_FEE;
    }
    
    private int currentAccountFee(int number){
    return bankAccount.getAmountOfMoney()-number;
    }
            
    public void setAge(int age) {
        this.age = age;
    }
    
    public void setCash(int cash) {
        this.cash = cash;
    }
    
    public int getAge() {
        return age;
    }
    
    public int getCash() {
        return cash;
    }
    
    public List<BankAccount> getBankAccountList() {
        return bankAccountList;
    }
    
    public List<DebitCard> getDebitCardList() {
        return debitCardList;
    }
    
    public List<CreditCard> getCreditCardList() {
        return creditCardList;
    }
    
    @Override
    public String toString() {
        return "Person{" + "age=" + age + ", cash=" + cash + ", bankAccounts=" + bankAccountList + '}';
    }
    
}
