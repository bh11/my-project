
package bh11_homework_15;


public class CreditCardCredit {
    
     private int loan;

    public CreditCardCredit(int loan) {
        this.loan = loan;
    }
     
    public int getLoan() {
        return loan;
    }

    public void setLoan(int loan) {
        this.loan = loan;
    }

    @Override
    public String toString() {
        return "CreditCardCredit{" + "loan=" + loan + '}';
    }
     
    
}
