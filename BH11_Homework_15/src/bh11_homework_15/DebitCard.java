
package bh11_homework_15;


public class DebitCard{
    
    /*
    Bankszámlához kapcsolódik, kártyaszámmal rendelkezik
    ○Igénylési díja egyszeri 5000 Ft
    ○Plusz pontért megvalósítható: kártyaszámgenerálás a következő osztály segítségével:https://github.com/grahamking/darkcoding-credit-card/blob/master/RandomCreditCardNumberGenerator.java
    ○A készpénzfelvétel díja a felveendő összeg 10%-a, de maximum 1000 Ft
    ○A kártyás vásárlás ingyenes
    */
    
    private int cardNumber;
    public static final int FEE_FOR_OPENING = 5000;
    public static final double WITHDRAW_CASH_FEE = 0.1;
    public static final int MAX_AMOUNT_OF_WITHDRAW_FEE = 1000;
    public static final double TRANSACTION_FEE = 0;
    

        public int cashWithdraw (int number){
            if (number*WITHDRAW_CASH_FEE<=MAX_AMOUNT_OF_WITHDRAW_FEE) {
                   return (int) (number+number*WITHDRAW_CASH_FEE);  
            }else{
            return 0;
            }       
    }
    

       public int getCardNumber() {
        return cardNumber;
    }
       
    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }



}
