
package Students;

import Person.Person;


public class Student extends Person{
    
    private String neptunCode;
    
    public Student(String neptunCode){
    super("Zsuga T. Gabriella");
    this.neptunCode = neptunCode;
    }
    
    public Student(){
    this("IINIH3");
    }

    public String getNeptunCode() {
        return neptunCode;
    }

    public void setNeptunCode(String neptunCode) {
        this.neptunCode = neptunCode;
    }

    @Override
    public String toString() {
        return super.toString() + " - " + neptunCode;
    }
   
    
}
