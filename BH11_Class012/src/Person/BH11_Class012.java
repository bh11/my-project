package Person;

import Students.Student;

public class BH11_Class012 {

    public static void main(String[] args) {

        /*
        
        2. feladat:
Írj Ember néven egy osztályt, aminek egy String típusú nev attribútuma (private) van. Rendelkezik equals()
és hashCode() metódusokkal is. Ez az osztály az ember package-ben legyen és van egy olyan egy
paraméteres konstruktora, ami eltárolja az ember nevét.
Hozz létre egy másik osztályt a hallgato csomagban Hallgato néven. A Hallgató osztály leszármazottja az
Ember osztálynak. A Hallgato osztályról a következőket tudjuk:
 van egy String típusú privát attribútuma: neptun
 van egy egyparaméteres konstruktora, ami egy neptun kódot és a Te nevedet tárolja el. Ebben a
konstruktorban maximum 1 = operátort használhatsz.
 van egy paraméter nélküli konstruktora, amiben a saját neptun kódodat tárolod el.
 Törekedj a super() és a this() használatára!
         */
    
    Object o = new Person("Béla");
    if (o instanceof Person){
    Person p = (Person) o;
        p.getName();
        
        System.out.println(p.getName());
    }

Student s = new Student();    
Object oo = new Student();

        System.out.println(s);
        System.out.println(oo);
    }
    
    

}
