package bh11_homework_06_minigame;

import java.util.Scanner;

public class BH11_Homework_06_MiniGame {

    static final int MIN_SIZE = 10;
    static final int MAX_SIZE = 15;

    static final int MIN_BOMBS = 8;
    static final int MAX_BOMBS = 20;
    static final int NUMBER_OF_GIFTS = 3;

    static int lifePoints = 5;
    static int howManySteps = 0;

    static final char PLAYER = '\u263A';
    static final char GOOD_BOMBS = '\u2606';
    static final char BAD_BOMBS = '\u2738';
    static final char GIFTS = '\u2661';
    static final char FINISH = '\u0024';
    static final char EMPTY = '\0';

    static int playerX = 0;
    static int playerY = 0;

    static final Scanner SCANNER = new Scanner(System.in);

    static int getNumber(int from, int to) {
        do {
            System.out.printf("Adj meg egy számot %d és %d között! ", from, to);
            if (SCANNER.hasNextInt()) {
                int number = SCANNER.nextInt();
                if (number >= from && number <= to) {
                    return number;
                }
            } else {
                jumpString();
            }

        } while (true);
    }

    static void jumpString() {
        SCANNER.next();
    }

    static void printField(char[][] field) {

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] == EMPTY) {
                    System.out.print(" ");
                } else {
                    System.out.print(field[i][j]);
                }
            }
            System.out.println();
        }
    }

    static void putPlayer(char[][] field) {
        field[playerX][playerY] = PLAYER;
    }

    static void play(int row, int col) {
        char[][] field = new char[row][col];
        putPlayer(field);
        putFinish(field);
        putGifts(field);
        putBombs(field);
        step(field);
        victoryOrDefeat();

    }
    
    static void putFinish(char[][] field){
    field[field.length-1][field[0].length-1]=FINISH;
    }
    
    static void putGifts(char[][] field){
        setGifts(field, NUMBER_OF_GIFTS, GIFTS);
        printField(field);
    }
    
    static void setGifts(char[][] field, int numberOfGifts, char character){
        for (int i = 0; i < numberOfGifts; i++) {
            int x = generateRandomNumber(0, field.length);
            int y = generateRandomNumber(0, field[0].length);
            if (field[x][y]==EMPTY) {
                field[x][y]= character;
            }else{
            i--;
            }
        }
    }
    static void putBombs(char[][] field) {
        int numberOfBombs = getNumber(MIN_BOMBS, MAX_BOMBS);
        int numberOfGoodBombs = generateGoodBombs(numberOfBombs);
        int numberOfBadBombs = numberOfBombs - numberOfGoodBombs;

        setBombs(field, numberOfGoodBombs, GOOD_BOMBS);
        setBombs(field, numberOfBadBombs, BAD_BOMBS);
        printField(field);
    }

    static int generateGoodBombs(int numberOfBombs) {
        return numberOfBombs / 3;
    }

    static void setBombs(char[][] field, int numberOfBombs, char character) {

        for (int i = 0; i < numberOfBombs; i++) {
            int x = generateRandomNumber(0, field.length);
            int y = generateRandomNumber(0, field[0].length);
            if (field[x][y] == EMPTY) {
                field[x][y] = character;
            } else {
                i--;
            }
        }

    }

    static int generateRandomNumber(int from, int to) {
        int number = (int) (Math.random() * (to - from) + from);
        return number;
    }


    static void step(char[][] field) {
        while (endOfGame(field)) {
            System.out.println("Mozgasd a játékost a w, a, s, d billentyűk lenyomásával!");
            char move = SCANNER.next().charAt(0);
            switch (move) {
                case 'w': moveUp(field); break;
                case 'a': moveLeft(field);break;
                case 's': moveDown(field); break;
                case 'd': moveRight(field);break;
                    }
            howManySteps++;
            }
    }

    static boolean endOfGame(char[][] field) {
        if (field[field.length-1][field[0].length-1]!=PLAYER && lifePoints > 0) {
            return true;
        }
        return false;
    }
    
    static void victoryOrDefeat(){
    if (lifePoints>0) {
        System.out.printf("Gratulálok, nyertél! %d életed maradt. \n", lifePoints);
        System.out.printf("A játékot %d lépésből sikerült megoldanod! \n", howManySteps);
        }else{
            System.out.println("Sajnos vesztettél :(");
        }
    }

    static void handleBombs(char[][] field, int x, int y) {
        if (field[x][y] == GOOD_BOMBS) {
            lifePoints--;
            System.out.println("Vesztettél!");
        } else if (field[x][y] == BAD_BOMBS) {
            lifePoints = 0;
        }
    }
    static void handleGifts(char[][] field, int x, int y) {
        if (field[x][y] == GIFTS) {
            lifePoints++;
        }
    }
    

    static void moveDown(char[][] field) {
        if (!playerIsInTheLastRow(field)) {
            handleBombs(field, playerX + 1, playerY);
            handleGifts(field, playerX+1, playerY);
            if (lifePoints > 0) {
                field[playerX][playerY] = EMPTY;
                playerX++;
                field[playerX][playerY] = PLAYER;
            } 
        }
               printField(field); 
    }

    static void moveUp(char[][] field) {
        if (!playerIsInTheFirstRow(field)) {

            handleBombs(field, playerX - 1, playerY);
            handleGifts(field, playerX-1, playerY);
            if (lifePoints > 0) {
                field[playerX][playerY] = EMPTY;
                playerX--;
                field[playerX][playerY] = PLAYER;
            }
        }
        printField(field);
    }

    static void moveRight(char[][] field) {
        if (!playerIsInTheLastColumn(field)) {
            handleBombs(field, playerX, playerY+1);
            handleGifts(field, playerX, playerY+1);
            if (lifePoints > 0) {
                field[playerX][playerY] = EMPTY;
                playerY++;
                field[playerX][playerY] = PLAYER;
            }
        }
        printField(field);
    }

    static void moveLeft(char[][] field) {
        if (!playerIsInTheFirstColumn(field)) {

            handleBombs(field, playerX, playerY-1);
            handleGifts(field, playerX, playerY-1);
            if (lifePoints > 0) {
                field[playerX][playerY] = EMPTY;
                playerY--;
                field[playerX][playerY] = PLAYER;
            }
        }
        printField(field);
    }

    static boolean playerIsInTheLastRow(char[][] field) {
        return playerX == field.length - 1;
    }

    static boolean playerIsInTheFirstRow(char[][] field) {
        return playerX == 0;
    }

    static boolean playerIsInTheLastColumn(char[][] field) {
        return playerY == field[0].length-1;
    }

    static boolean playerIsInTheFirstColumn(char[][] field) {
        return playerY == 0;
    }

    public static void main(String[] args) {
        /*
        A felhasználó megad két egész számot 10 és 15 között. 
        Ebből generálunk egy 2D-s pályát. 
        Generáljunk valamennyi bombát 8 és 20 között.
        2 típusú bamba van: az egyikre lépve meghalunk, a másikra lépve csak életet veszítünk.
        70%-ban halált okoz, 30%-ban csak életet veszítünk. 
        A kezdeti életek száma 5.
        Van egy játékosunk, akivel csak jobbra és lefelé tudunk lépni.
        A játékos kezdetben a 0:0-án áll.
        A cél, hogy eljussunk az utolsó sorba úgy, hogy nem halunk meg. 
        Ha eljutottunk az utolsó sorba, íjuk ki, hogy hány életunk maradt és hány lépésből oldottuk meg. 
         */
        int row = getNumber(MIN_SIZE, MAX_SIZE);
        int col = getNumber(MIN_SIZE, MAX_SIZE);
        play(row, col);

    }

}

