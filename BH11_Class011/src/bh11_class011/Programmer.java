
package bh11_class011;


public class Programmer extends Employee{
    
    private boolean homeOffice;
    
    public Programmer(Name name, double salary) {
        super(name, salary);
    }
    
    public boolean isHomeOffice(){
    return homeOffice;
    }
    
    @Override
        public double getSalary() {
        return Double.MAX_VALUE;
    }

    @Override
    public String toString() {
        return "Programmer{"  + "name=" + name + ", salary=" + salary + "homeOffice=" + homeOffice + '}';
    }
  
}
