/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh11_class011;

/**
 *
 * @author gabri
 */
public class Manager extends Employee{
    
    String car;
    public Manager(Name name, double salary, String car) {
        super(name, salary);
    this.car = car;
    }
    
    @Override
        public void setSalary(double salary) {
        if (salary < 0 || salary > 1000000000) {
            this.salary = 0;
            System.out.println("Wrong salary, salary is set to 0");
        } else {
           this.salary = salary;
        }
    }

    @Override
    public String toString() {
        return "Manager{" + "name=" + name + ", salary=" + salary +'}';
    }
        
        
    
}
