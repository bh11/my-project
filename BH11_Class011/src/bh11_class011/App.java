package bh11_class011;

public class App {

    public static void main(String[] args) {

        Company company = new Company();
        Company.raiseIndicator = 1.2; // Mivel a raiseIndicator statikus, ezért az osztályon keresztül hozzuk létre

        Employee employee = new Employee("Béla", "Kiss", 300000.0);
//        employee.firstName = "Bela";
//        employee.lastName = "Kiss";
//        employee.salary = 300000.0;
        company.addEmployee(employee);
        System.out.println(employee.getName().getFullName());
        System.out.println(employee.getSalary() + " Ft");

        employee.increseSalary(company.raiseIndicator);
        System.out.println(employee.getSalary() + " Ft");

        Employee employee2 = new Employee("Ádám", "Nagy", 200000.0);
//        employee2.firstName = "Adam";
//        employee2.lastName = "Nagy";
//        employee2.salary = 200000.0;

        System.out.println(employee2.getName().getFullName());
        company.addEmployee(employee2);
        System.out.println(employee2.getSalary() + " Ft");
        employee2.increseSalary(company.raiseIndicator);
        System.out.println(employee2.getSalary() + " Ft");

        Employee employee3 = new Employee("Zoltán", "Gábor", "Kovács", 250000.0);
        company.addEmployee(employee3);
        System.out.println(employee3.getName().getFullName());
        System.out.println(employee3.getSalary() + " Ft");
        employee3.increseSalary(company.raiseIndicator);
        System.out.println(employee3.getSalary() + " Ft");

//        for(int i = 0; i<employees.length; i++){
//        employees[i].increseSalary();
//            System.out.println(employees[i].name.getFullName()+ " incresed salary: "+ employees[i].salary+" Ft");
//        }
//        
//        for(Employee emp: employees){
//            System.out.println(emp.name.getFullName());
//        }
//        
        Name name = new Name("Eszter", "Móricz");
        Employee employee4 = new Employee(name, 500000.0);
        System.out.println(employee4.getName().getFullName());
        company.addEmployee(employee4);

        Employee employee5 = new Employee(new Name("Lajos", "Lajvér"), 150000.0);
        System.out.println(employee5.getName().getFullName());
        company.addEmployee(employee5);

        System.out.println();
        System.out.println();

        company.printEmployesWhithSalaries();
        System.out.println();
        company.increaseSalaries();
        company.printEmployesWhithSalaries();

        Employee emp10 = new Manager(new Name ("Tamas", "H"), 20000, "BMW");
    }
}
