
package hu.braininghub;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class DeleteEmployeeServlet extends HttpServlet {

    private EmployeeService employeeService = new EmployeeService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       employeeService.deleteEmployeeByName(request.getParameter("name"));
       request.setAttribute("employees", employeeService.getEmployees());
       request.getRequestDispatcher("employees.jsp").forward(request, response);
    }
   
}
