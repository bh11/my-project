package hu.braininghub;

import java.util.ArrayList;
import java.util.List;

public class EmployeeService {

    private static List<Employee> employees = new ArrayList<>();

    public static List<Employee> getEmployees() {
        return employees;
    }

    public void addEmployee(Employee emp) {
        employees.add(emp);
    }

    public void deleteEmployeeByName(String name) {
        int index = getIndexByName(name);
        if (index != -1) {
            employees.remove(index);
        }
    }

    private int getIndexByName(String name) {
        for (int i = 0; i < employees.size(); i++) {
            if (name.equals(employees.get(i).getName())) {
                return i;
            }
        }
        return -1;
    }
}
