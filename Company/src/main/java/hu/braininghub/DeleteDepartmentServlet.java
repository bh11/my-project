/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class DeleteDepartmentServlet extends HttpServlet {

   private DepartmentService departmentService = new DepartmentService();
   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    departmentService.deleteDepartmentById(request.getParameter("ID"));
    request.setAttribute("departments", this.departmentService.getDepartment());
      request.getRequestDispatcher("department.jsp").forward(request, response);
    }

   
}
