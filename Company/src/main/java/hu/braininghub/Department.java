/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub;


public class Department {
    
    private String name;
    private String ID;
    
    
    //Hozzáadni, törölni, listázni
    //Employeehoz legyen egy új sor, ami a department neve
    //A táblázatban lehessen szűrni, hogy csak a az adott departmentben lévő employeekat lehessen látnni

    public Department(String name, String ID) {
        this.name = name;
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
    
    
}
