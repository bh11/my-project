
package hu.braininghub;

import java.util.ArrayList;
import java.util.List;


public class DepartmentService {
    private static List<Department> departments = new ArrayList<>();
    
       public static List<Department> getDepartment() {
        return departments;
    }
       
        public void addDepartment(Department department) {
        departments.add(department);
    }
      
        public void deleteDepartmentById(String id) {
        int index = getIndexById(id);
        if (index != -1) {
            departments.remove(index);
        }
    }
       private int getIndexById(String id) {
        for (int i = 0; i < departments.size(); i++) {
            if (id.equals(departments.get(i).getID())) {
                return i;
            }
        }
        return -1;
    }
        

}
