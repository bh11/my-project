package hu.braininghub;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddEmployeeServlet extends HttpServlet {

    private EmployeeService employeeService = new EmployeeService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.employeeService.addEmployee(
                new Employee(request.getParameter("name"),
                        request.getParameter("address"),
                        Integer.valueOf(request.getParameter("salary"))));
        request.setAttribute("employees", this.employeeService.getEmployees());
        request.getRequestDispatcher("employees.jsp").forward(request, response);
    }

}
