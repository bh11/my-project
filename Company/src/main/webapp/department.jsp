<%-- 
    Document   : department
    Created on : 2020.01.22., 20:49:42
    Author     : gabri
--%>

<%@page import="hu.braininghub.Department"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% List<Department> department = (List<Department>) request.getAttribute("departments"); %>
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>ID</th>
                </tr>
            </thead>
            <tbody>
            <h2>Department list</h2>
            <% for (int i = 0; i < department.size(); i++) {%>
            <tr>
                <td><%= department.get(i).getName()%></td>
                <td><%= department.get(i).getID()%></td>
            </tr>
            <% }%>
        </tbody>
    </table>
    <h2>Add new Department</h2>
    <form method="post" action="AddDepartmentServlet" >
        <p><input type="post" name="name" /></p>
        <p><input type="post" name="ID" /></p>
        <p><input type="submit" value="Add department" /> 
    </form>
    <h2>Delete Department</h2>
    <form method="post" action="DeleteDepartmentServlet">
        <p><input type="post" name="name"/></p>
        <input type="submit" value="Delete department by ID"/>
    </form>
</body>
</html>
