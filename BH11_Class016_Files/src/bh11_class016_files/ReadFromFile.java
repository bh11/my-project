package bh11_class016_files;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadFromFile {

    public String readFirstLine() {
        String firstRow = null;

        try (BufferedReader br = new BufferedReader(new FileReader("bh.txt"))) {
        firstRow = br.readLine();
        
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        } catch (IOException e) {
            System.out.println(e);
        }
        return firstRow;
    }
}
