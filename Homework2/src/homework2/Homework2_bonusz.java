/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework2;

import java.util.Scanner;

/**
 *
 * @author Gabriella
 */
public class Homework2_bonusz {

    public static void main(String[] args) {
        /*
         Bónusz I.: 
         kérjünk be a felhasználótól a másodfokú egyenlet paramétereit, és oldjuk meg az egyenletet. 
         Segítség: gyököt az alábbi módon számítunk java-ban: double valtozo = Math.sqrt(szam), 
         pl. ha 5 gyökére vagyunk kíváncsiak: double eredmeny = Math.sqrt(5); 
         A Math a java.lang része, így nem szükséges importálni, egyből használhatjuk.
         */

        Scanner scanner = new Scanner(System.in);

        int a;
        int b;
        int c;

        System.out.println("Add meg a másodfokú egyenlet paramétereit!");
        System.out.println("A függvény alakja: a*x2 + b*x + c");
        a = scanner.nextInt();
        b = scanner.nextInt();
        c = scanner.nextInt();
        double x1;
        double x2;
        x1 = ((-b) + Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (a * 2.0);
        x2 = ((-b) - Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (a * 2.0);
        System.out.println("x1= " + x1);
        System.out.println("x2= " + x2);

    }

}
