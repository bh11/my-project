/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework2;

import java.util.Scanner;

public class Homework22 {

    public static void main(String[] args) {

        /*
         II. Calculator: 
         1. Kérjünk be a felhasználótól két egész számot + egy műveletet, 
         a művelet (+, -, *, /, %) ezeket mentsük megfelelő változókba
         2. írjuk ki a felhasználónak a művelettől függően a végeredményt, ügyelve az alábbiakra: 
         - osztás esetén legyenek megjelenítve a tizedesjegyek
         - 0-val való osztás esetén írja ki a felhasználónak hogy 0-val nem osztunk! 
         - 0-val való maradékképzés esetén írjuk ki a felhasználónak hogy 0-val nem képzünk maradékot!
         */
        Scanner scanner = new Scanner(System.in);

        System.out.println("Írj be egy műveleti jelet és két számot!");
        
        System.out.println("Első szám:");
        int n1 = scanner.nextInt();
        System.out.println("Második szám:");
        int n2 = scanner.nextInt();
        scanner.skip("\\n");
System.out.println("Művelet (+, -, *, /, %):");
        char operator = scanner.nextLine().charAt(0);
        
        if (operator == '+') {
            System.out.println("A számok összege: " + (n1 + n2));
        } else if (operator == '-') {
            System.out.println("A számok különbsége: " + (n1 - n2));
        } else if (operator == '*') {
            System.out.println("A számok szorzata: " + (n1 * n2));
        } else if (operator == '/') {
            if (n2 == 0) {
                System.out.println("0-val való osztás nem értelmezhető");
            } else {
                System.out.println("A számok hányadosa: " + (double) n1 / n2);
            }
        } else if (operator == '%') {
            if (n2 == 0) {
                System.out.println("Nullával nem képzünk maradékot!");
            } else {
                System.out.println("A számok maradéka: " + (n1 % n2));
            }
        } else {
            System.out.println("Nem jó műveleti jelet adtál meg!");

        }

    }
}
