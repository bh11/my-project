/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework2;

import java.util.Scanner;

/**
 *
 * @author Gabriella
 */
public class Homework2_bonusz2 {

    public static void main(String[] args) {
        /*
         Bónusz II.: ternary-ba ternary :-)
         Oldjuk meg a szökőéves feladatot ternarykat egymásba ágyazva
         */

        Scanner scanner = new Scanner(System.in);

        int year;

        System.out.println("Íj be egy évszámot!");
        year = scanner.nextInt();

        String leapYear;

        leapYear = (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0) ? "Szökőév" : "Nem szökőév";
        System.out.println(leapYear);
    }

}
