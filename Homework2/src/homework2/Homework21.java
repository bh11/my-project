package homework2;

import java.util.Scanner;

public class Homework21 {

    public static void main(String[] args) {
        /*
         I. kérjünk be a felhasználótól 3 egész számot, majd: 
         - számoljuk ki az átlagát és írjuk ki 
         - keressük meg benne a legnagyobb számot és írjuk ki
         - döntsük el, lehet-e háromszög (bármely kettő szám összege nagyobb mint a harmadik)
         */
        System.out.println("************ 1. FELADAT ************");
        Scanner scanner = new Scanner(System.in);

        int a;
        int b;
        int c;
        System.out.println("Írd be az első számot!");
        a = scanner.nextInt();
        System.out.println("Írd be a második számot!");
        b = scanner.nextInt();
        System.out.println("Írd be a harmadik számot!");
        c = scanner.nextInt();

        double avg = (double) (a + b + c) / 3;
        System.out.println("A számok átlaga: " + avg);
        if (a > b && a > c) {
            System.out.println("A legnagyobb szám: " + a);
        } else if (b > a && b > c) {
            System.out.println("A legnagyobb szám: " + b);
        } else {
            System.out.println("A legnagyobb szám: " + c);
        }

        if (a + b > c && a + c > b && b + c > a) {
            System.out.println("Lehet belőle háromszöget szerkeszteni :)");
        } else {
            System.out.println("Nem lehet belőle háromszöget szerkeszteni :(");
        }

    }

}
