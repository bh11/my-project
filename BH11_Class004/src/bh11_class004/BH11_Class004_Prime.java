package bh11_class004;

import java.util.Scanner;

public class BH11_Class004_Prime {

    public static void main(String[] args) {

        /*
        Kérj be a felhasználótól egy számot és írja ki, hogy prím vagy nem prím
         */
        Scanner scanner = new Scanner(System.in);

        System.out.println("Kérlek írj be egy számot!");
        int i = scanner.nextInt();

        int counter = 0;
        for (int j = 1; j <= Math.sqrt(i); j++) {
            if (i % j == 0) {
                counter++;
            }
        }
        if (counter == 1 && i != 1) {
            System.out.println("A szám prímszám");
        } else {
            System.out.println("A szám nem prímszám");
        }

        ////////// MÁSIK MEGOLDÁS ///////////////
        boolean isPrime = true;

        for (int j = 2; j <= Math.sqrt(i); j++) {
            if (i % j == 0) {
                isPrime = false;
            }

        }
        if (!isPrime || i == 1) {
            System.out.println("Nem prím");
        } else {
            System.out.println("Prím");
        }
    }

}
