package bh11_class004;

public class BH11_Class004_BigestNumber {

    public static void main(String[] args) {

        int[] numbers = {2, 8, 45, 23, 76, 21, 25, 6, 4, 23};

        int max = Integer.MIN_VALUE; //Adjuk meg a lehető legkisebb értéket

        //Azt is megtehetjük, hogy megadjuk a tömb első elemét numbers[0]
        int max2 = numbers[0]; // ÉRDEMES EZT HASZNÁLNI

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > max) {
                max = numbers[i];
            }
        }
        System.out.println("A legnagyobb elem: " + max);

    }
}
