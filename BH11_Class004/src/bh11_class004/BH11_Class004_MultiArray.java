/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh11_class004;

/**
 *
 * @author gabri
 */
public class BH11_Class004_MultiArray {

    public static void main(String[] args) {

        int[][] multiArray = new int[10][10];
        for (int i = 0; i < multiArray.length; i++) {
            for (int j = 0; j < multiArray[i].length; j++) {
                multiArray[i][j] = i * j;
            }
        }
        for (int i = 0; i < multiArray.length; i++) {
            for (int j = 0; j < multiArray[i].length; j++) {
                System.out.print(" " + multiArray[i][j]);
            }
            System.out.println();
        }
        int[][] ma = {{2, 3}, {1, 5}};
    }

}
