package bh11_class004;

public class BH11_Class004_Arrays2 {

    public static void main(String[] args) {

        int[] numbers = {2, 3, 4};
        int[] array = numbers;
        array[1]++;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(array[i] + "-" + numbers[i]);
        }

        array = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            array[i] = numbers[i];
        }

        System.out.println();

        array[1]++;
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(array[i] + "-" + numbers[i]);
        }

    }

}
