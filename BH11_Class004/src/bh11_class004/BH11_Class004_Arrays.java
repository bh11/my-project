package bh11_class004;

public class BH11_Class004_Arrays {

    public static void main(String[] args) {

        // típus[] nev = new típus[elemszám]
        int[] num1 = new int[10];
        int[] num2 = {2, 8, 6, 11};
        // System.out.println(num2[8]);
        // ArrayIndexOutOfBoundsException a tömb egy olyan elemére való hivatkozás, ami nem létezik.
        // A tömb elemszáma fix

        System.out.println(num2[0]);
        System.out.println(num2[1]);
        System.out.println(num2[2]);

        for (int i = 0; i < num2.length; i++) {
            System.out.println(num2[i]);
        }
        double[] num3 = {2.4, 32.2, 3, 12.8};
        float[] num4 = {2.32f, 2f, 32.87f};

        for (int i = 0; i < num3.length; i++) {
            System.out.println(num3[i]);
        }
        for (int i = 0; i < num4.length; i++) {
            System.out.println(num4[i]);
        }

        System.out.println();
        System.out.println();

        int[] num5 = null;
        //System.out.println(num5[0]);
        //java.lang.NullPointerException a tömbnek nincs értéke

        int[] num6 = new int[10]; //minden eleme defaultból 0

    }
}
