package bh11_class004;

public class BH11_Class004_VeletlenSorrend {

    public static void main(String[] args) {

        // 1-től 10-ig írjuk ki random a számokat!

        int[] array = new int[10];
        int random;
        int countOfNumber = 0;

        while (countOfNumber < 10) {
            boolean counter = false;
            random = (int) ((Math.random() * 10) + 1);
            for (int i = 0; i < array.length; i++) {

                if (array[i] == random) {
                    counter = true;
                }
            }
            if (!counter) {
                array[countOfNumber] = random;
                countOfNumber++;

            }

        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(" " + array[i]);
        }
        System.out.println();
    }

}
