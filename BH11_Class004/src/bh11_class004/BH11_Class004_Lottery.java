package bh11_class004;

public class BH11_Class004_Lottery {

    public static void main(String[] args) {

        /*
        Csináljunk egy 5-ös lottó számgenerátort!
         */
        int[] lottery = new int[5];
        int countOfNumber = 0;
        int counter = 0;

        while (countOfNumber < 5) {

            int randomNumbers = (int) ((Math.random() * 10) + 1);
            boolean containsItem = false;
            for (int i = 0; i < countOfNumber; i++) {
                if (lottery[i] == randomNumbers) {
                    counter++;
                    containsItem = true;
                }
            }
            if (!containsItem) {
                lottery[countOfNumber] = randomNumbers;
                countOfNumber++;
            }
        }

        for (int i = 0; i < lottery.length; i++) {
            System.out.println(lottery[i]);
        }
        System.out.println("Az ismétlődések száma: " + counter);
    }

}
