package bh11_class004;

public class BH11_Class004_Stars {

    public static void main(String[] args) {

        int number = 7;
        for (int i = 0; i < number; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }

        // 1-től 100-ig összeszorozva a számokat hány 0-ra fog végződni?
//        
//        int num1 = 125;
//        int ot =0;
//        
//        while (num1%5==0){
//        ot++;
//        num1 = num1/5;
//        }
//        System.out.println(ot);
        int five = 0;
        int end = 11;
        for (int i = 5; i < end; i++) {
            int num = i;
            while (num % 5 == 0) {
                five++;
                num = num / 5;
            }
        }
        System.out.println("Ötös: " + five);
    }
}
