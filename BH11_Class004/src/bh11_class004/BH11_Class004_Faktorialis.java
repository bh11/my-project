/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh11_class004;

/**
 *
 * @author gabri
 */
public class BH11_Class004_Faktorialis {

    public static void main(String[] args) {

        // n!
        int number = 10;
        int faktorialis = 1;

        for (int i = 1; i <= number; i++) {
            faktorialis *= i;
        }
        System.out.println(faktorialis);
    }

}
