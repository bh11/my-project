package bh11_class004;

/**
 *
 * @author gabri
 */
public class BH11_Class004_Exercise {

    public static void main(String[] args) {
        /*
        Van egy 10 elemű tömbünk, ami 1 és 100 között vesz fel értékeket. 
        Írjuk ki az elem maximumát és helyét
         */

        int[] array = new int[10];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10 + 1);
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(+array[i] + " ");
        }
        System.out.println();

        int max = array[0];
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
                index = i;
            }
        }
        System.out.println("A legnagyobb elem: " + max + " indexe: " + index);

    }

}
