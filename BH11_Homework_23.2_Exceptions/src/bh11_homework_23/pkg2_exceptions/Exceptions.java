/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh11_homework_23.pkg2_exceptions;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gabri
 */
public class Exceptions {
    
    public static void arrayStoreException() {
        Object[] x = new String[3];
        x[2] = 2;

    }

    public static void concurrentModificationException() {

        List<Integer> list = new ArrayList<>();
        int a = 1;
        int b = 2;
        int c = 3;
        int d = 4;

        list.add(a);
        list.add(b);
        list.add(c);
        list.add(d);
        
        list.forEach(f->{list.remove(f);});

        
    }
    
}
