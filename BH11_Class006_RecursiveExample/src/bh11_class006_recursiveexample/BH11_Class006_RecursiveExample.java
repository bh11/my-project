
package bh11_class006_recursiveexample;


public class BH11_Class006_RecursiveExample {

   static void m(int a){
       
       if (a==0) {
           return;
       }
       System.out.println(a);
       m(--a);
   }
    public static void main(String[] args) {
        
        m(10);
    }
    
}
