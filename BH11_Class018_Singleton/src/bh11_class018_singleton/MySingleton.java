package bh11_class018_singleton;

public class MySingleton {

    private static MySingleton instance;

    private MySingleton() { //azzal, hogy private a konstruktor, letiltjuk a példányosítást

    }

    public static MySingleton getInstance() {
        if (instance == null) {
            instance = new MySingleton();
        }
        return instance;
    }

    public boolean validateAge(int age) {
        if (age < 14) {
            return false;
        }
        return true;
    }

}
