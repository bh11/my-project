
package bh11_class009;


public class BH11_Class009_RandomNumbers {
    
    static int[] randomNumbers(){
     int[] array = new int[100];
     int start = -1;
     int counter = 10;
        for (int i = 0; i < 100; i++) {
            
         int number = (int) (Math.random() * (counter));
            if (start<number) {
                start = number;
                array[i]= number;
            }else{
            i--;
            counter+=10;
            }
        }
        return array;
    }
        static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]+" ");
        }
        System.out.println();
    }
    /*
    Generálj 100 számot úgy, hogy minden eleme nagyobb, mint a korábban kisorsolt szám
    */
    
   
    public static void main(String[] args) {

        printArray(randomNumbers());
    }
 
}
