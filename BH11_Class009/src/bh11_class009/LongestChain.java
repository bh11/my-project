
package bh11_class009;

public class LongestChain {

    public static void main(String[] args) {
        
        int[] arr = {1, 2, 3, 1, 2, 5, 6, 8, 9, 7, 8, 9, 3, 4, 5, 6, 1, 2, 3, 6, 7, 8, 10, 45, 66, 77};
        int index = -1;
        int length = 0;
        for (int i = 0; i < arr.length; i++) {
            int count = 1;
            boolean stop = false;
            for (int j = i + 1; !stop && j < arr.length; j++) {
                if (arr[j - 1] < arr[j]) {
                    count++;
                } else {
                    stop = true;
                }
            }
            if (count > length) {
                length = count;
                index = i;
            }
        }
        for (int i = index; i < length + index; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
    
    

