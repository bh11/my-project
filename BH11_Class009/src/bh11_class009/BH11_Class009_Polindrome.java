
package bh11_class009;

import java.util.Scanner;



public class BH11_Class009_Polindrome {
//    
//    static int length(String s){
//        int length = 0;
//        for (int i = 0; i < s.length(); i++) {
//            if (s.charAt(i)!=' ') {
//                length++;
//            }
//        }
//        return length;
//    }
//    
    static String withoutSpace(String s){
    String newS = s.replaceAll("\\s","");
    return newS;
    }
    
    static int countLetters(String s){
    int length = 0;
        for (int i = 0; i < s.length(); i++) {
            length++;
        }
        return length;
    }
    
    static char[] stringToCharArray(String s){
    int length = countLetters(s);
    char[] letterByletter = new char[length];
        for (int i = 0; i < letterByletter.length; i++) {
            letterByletter[i]=s.charAt(i);
        }
        return letterByletter;
    }
    
        static void printArray(char[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]+" ");
        }
        System.out.println();
    }

        static char[] backward(char[] elore){
            char[] vissza = new char[elore.length];
            for (int i = elore.length-1; i >= 0; i--) {
                vissza[i]=elore[i];
            }
         return vissza;   
        }
        
        static boolean isPolindrome(char[]elore, char[]hatra){
            int index = (hatra.length-1);
            while(index>=0){
            for (int i = 0; i < elore.length; i++) {
                if (elore[i]!=hatra[index]){
                return false;
                }else{
                index--;    
                }
            }
            }
            return true;
        }
        
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        String sentence= scanner.nextLine();
        char[] polindromeOrNot;
  
        String newSentence = withoutSpace(sentence);
        polindromeOrNot= stringToCharArray(newSentence);
        char[] backward = backward(polindromeOrNot);
        System.out.println(isPolindrome(polindromeOrNot, backward));

    }
    
}
