/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh11_class009;

import java.util.Scanner;

/**
 *
 * @author gabri
 */
public class BH11_Class009_LowerCase {

    static void printArray(char[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    static char[] LowerCase(String sentence) {
        char[] lowerCase = new char[sentence.length()]; //Mivel a kisbetűk csak 97-től 122-ig vannak
        int index = 0;
        for (int i = 0; i < sentence.length(); i++) {
            if (sentence.charAt(i)>97 && sentence.charAt(i)<122) {
                lowerCase[index]=sentence.charAt(i);
                index++;
            }
        }
        return lowerCase;
    }

    public static void main(String[] args) {

        /**
         * Írjunk egy olyan metódust, ami kap egy karakter tömböt. Visszatérési
         * értékként egy tömbbel tér vissza, amely a paraméterben kapott tömbben
         * található kisbetűket tartalmazza.
         */
        
        Scanner scanner = new Scanner(System.in);
        printArray(LowerCase(scanner.nextLine()));
    }
}
