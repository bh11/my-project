package bh11_class009;

public class BH11_Class009_Prime {

    static boolean isPrime(int number) {
        int counter = 0;
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                counter++;
            }
        }
        if (counter > 2 || number == 1) {
            return false;
        } else {
            return true;
        }
    }

    static int[] fillPrime(int[] array) {
        int counter = 0;
        int number = 2;
        while (counter < 1000) {
            if (isPrime(number)) {
                array[counter]=number;
                counter++;
            }
            number++;
        }
        return array;
    }

    static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {

        /*
        Írjunk prímtényezős felbontást két tömb felhasználásával egy felhasználótól bekért 100 fölötti egész számra.
        Egyik tömb: prímszámok, amelyekkel osztani tudunk
        Másik tömb: adott prímszámhoz megmondja, hányszor tudtunk vele osztani
         */
        int[] prime = new int[1000];
        int[] dividedByPrime = new int[1000];
        int index = 0;
        printArray(fillPrime(prime));
        
        
        int number = 4000681;
        while(number>1){
            if (isPrime(number)) {
                number = -1;
            }
            if (number%prime[index]==0) {
                dividedByPrime[index]+=1;
                number/=prime[index];
            }else{
            index++;
        }
        }
        
        printArray(dividedByPrime);

        
        }

    
}
