
package bh11_class009;


public class Prime {

    
    static boolean isPrime(int number) {  
        int counter = 0;
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                counter++;
            }
        }
        
        return counter == 2;
    }
    
    static void doPriming(int number, int[] primeNumbers, int[] count) {
        int counter = 0;
        for (int i = 2; i <= number; i++) {
            if (number%i == 0 && isPrime(i)) {
                primeNumbers[counter] = i;
                
                int copiedNumber = number;
                while (copiedNumber % i == 0) {
                    count[counter]++;
                    copiedNumber /= i;
                }
                
                counter++;
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int number = 10003; // Most úgy vesszük, hogy ezt a felhasználó adta meg.
        int[] primeNumbers = new int[1000];
        int[] count = new int[1000];
        
        doPriming(number, primeNumbers, count);
        
        for (int i = 0; primeNumbers[i] != 0 && i < primeNumbers.length; i++) {
            System.out.printf("%d - %d\n", primeNumbers[i], count[i]);
        }
        
        
    }
    
}

