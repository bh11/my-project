
package bh11_class009;

public class BH11_Class009_Array {

    static int generateRandomNumber(int from, int to) {
        int number = (int) (Math.random() * (to - from + 1) + from);
        return number;
    }

    static void fillArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = generateRandomNumber(0, 255);
            }
        }
    }

    static void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    static void printArray(double[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.printf("%.2f ", array[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }

    static double[][] copyArray(int[][] array) {
        double[][] copy = new double[array.length][array[0].length];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                copy[i][j] = array[i][j];
            }
        }
        return copy;
    }

    static double avg(int[][] array, int row, int col) {
        int sum = 0;
        for (int i = row; i < row + 3; i++) {
            for (int j = col; j < col + 3; j++) {
                sum += array[i][j];
            }
        }
        return sum / 9D;
    }

    static void doAvg(int[][] array, double[][] array2) {
        for (int i = 1; i < array.length - 1; i++) {
            for (int j = 1; j < array[i].length - 1; j++) {
                array2[i][j] = avg(array, i - 1, j - 1);
            }
        }

    }

    public static void main(String[] args) {
        /*
        Generáljunk egy számot 10 és 15 között (15 is benne lehet). Ennek megfelelően hozzunk létre egy négyzetes 2D tömböt.
        A tömböt töltsük fel 0 és 255 közötti egész számokkal.
        Egy ugyanakkora méretű másik tömböt hozzunk létre.
        Ennek első és utolsó sora, valamint első és utolsó oszlopa az eredeti tömbben lévő értékekkel egyezik meg.
        Minden más eleme az eredeti tömbben lévő szomszédos elemek átlaga. 3 × 3-as résztömb átlagolása.
        Jelenítsük meg a tömböt.
         */

        int size = generateRandomNumber(10, 15);
        int[][] array = new int[size][size];
        fillArray(array);
        double[][] array2 = copyArray(array);
        doAvg(array, array2);
        printArray(array);
        printArray(array2);
    }
}
