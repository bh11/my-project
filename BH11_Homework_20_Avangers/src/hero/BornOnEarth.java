
package hero;

import ability.Flying;
import stone.StoneType;


public class BornOnEarth extends AbstractHero implements Flying{
    private final IdentityCard identityCard;
    
    public BornOnEarth(String name, int power, StoneType stone, IdentityCard identityCard){
    super(name, power, stone);
    this.identityCard = identityCard;
    }
    
   @Override
   public void fly(){
       System.out.println("I can fly.");
   }

    public IdentityCard getIdentityCard() {
        return identityCard;
    }

    @Override
    public String toString() {
        return super.toString() + "BornOnEarth{" + "identityCard=" + identityCard + '}';
    }
    
}
