
package read;

import hero.AbstractHero;
import hero.BornOnEarth;
import java.util.Optional;
import java.util.OptionalInt;
import store.Fleet;
import store.Ship;


public class Report {
    
    private final Fleet fleet;

    public Report() {
        this.fleet = LineParser.getStore();
    }
    
    public void generateReport(){
        reportNumberOfHeroesBornOnTheEarth();
        reportPassportNumbers();
        reportStrongestHero();
    }
    
    public void reportNumberOfHeroesBornOnTheEarth() {
      long numberOfHeroes=  fleet.getAllHeroesInTheShips().stream()
                .filter(e-> e instanceof BornOnEarth)
                .count();
        System.out.println("A földön születettek száma: "+numberOfHeroes);
    }
    
    public void reportStrongestHero() {
        OptionalInt heroPower = fleet.getAllHeroesInTheShips().stream()
                .mapToInt(h->h.getPower())
                .max();
        
        System.out.println("A legerősebb Bosszúálló ereje: "+ heroPower);
    }
    
    public void reportPassportNumbers() {
        
                fleet.getAllHeroesInTheShips().stream()
                .filter(h -> h instanceof BornOnEarth)
                .map(hero->((BornOnEarth) hero).getIdentityCard())
                .forEach(System.out::println);                
    }
    
}
