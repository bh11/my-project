package store;

import hero.AbstractHero;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Ship {

    public static final int MAX_SIZE = 4;
    public static final int INDEX_IN_NAME = 1;
    private final List<AbstractHero> heroes = new ArrayList<>();

    public void add(AbstractHero hero) {
        if (isEmptySeat()) {
            heroes.add(hero);
        }
        orderBySecondCharacterOfName();
    }

    private void orderBySecondCharacterOfName() {
//Anonymus osztály létrehozásával
        Comparator<AbstractHero> cmp = new Comparator<AbstractHero>() {
            @Override
            public int compare(AbstractHero hero1, AbstractHero hero2) {
                return hero1.getName().charAt(INDEX_IN_NAME) - hero2.getName().charAt(INDEX_IN_NAME);
            }
        };
        Collections.sort(heroes, cmp);
    }

    private void orderBySecondCharacterOfName2() {
        Collections.sort(heroes, new Comparator<AbstractHero>() {
            @Override
            public int compare(AbstractHero hero1, AbstractHero hero2) {
                return hero1.getName().charAt(INDEX_IN_NAME) - hero2.getName().charAt(INDEX_IN_NAME);
            }
        });
    }

    private void orderBySecondCharacterOfName3() {
        Collections.sort(heroes, (AbstractHero hero1, AbstractHero hero2) -> {
            return hero1.getName().charAt(INDEX_IN_NAME) - hero2.getName().charAt(INDEX_IN_NAME);
        });
    }

    private void orderBySecondCharacterOfName4() {
        Collections.sort(heroes, (hero1, hero2)
                -> hero1.getName().charAt(INDEX_IN_NAME) - hero2.getName().charAt(INDEX_IN_NAME));
    }

    public List<AbstractHero> getHeroes() {
        return heroes;
    }


    public boolean isEmptySeat() {
        return heroes.size() < MAX_SIZE;
    }

    @Override
    public String toString() {
        return "Ship{" + "heroes=" + heroes + '}';
    }
}
