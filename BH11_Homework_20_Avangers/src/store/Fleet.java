package store;

import hero.AbstractHero;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Fleet {
    
    private List<Ship> ships = new ArrayList<>();
    
    public void add(AbstractHero hero) {
        findShip().add(hero);
    }
    
    @Override
    public String toString() {
        return "Fleet{" + "ships=" + ships + '}';
    }
    
    private Ship findShip() {
       Ship ship = findLastShip();
        if (ship.isEmptySeat()) {
            return ship;
        }else{
        ships.add(new Ship());
        return ships.get(ships.size()-1);
        }
    }
    
    private Ship findLastShip(){
     if (ships.isEmpty()) {
            ships.add(new Ship());
        }
        return ships.get(ships.size()-1);
    }
    
         public Set<AbstractHero> getAllHeroesInTheShips() {
             
            return ships.stream()
                    .map(ship->ship.getHeroes())
                    .collect(Collectors.toSet())
                    .stream()
                    .flatMap(heroes->heroes.stream())
                    .collect(Collectors.toSet());             
    }


    
}
