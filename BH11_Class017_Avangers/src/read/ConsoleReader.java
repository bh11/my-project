
package read;

import exceptions.InvalidNameAvangersException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConsoleReader {
    
    private static final String STOP = "EXIT";
    private LineParser parser = new LineParser();
    
    public void read(){
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
            
            while(true){
            String line = br.readLine();
            
                if (line == null || STOP.equals(line)) {
                    break;
                }
                parser.process(line);
            }
        } catch (IOException ex) {
            System.out.println(ex);
        } catch (InvalidNameAvangersException ex) {
            System.out.println(ex.getMessage());
        }
        parser.print();
    }
    
}
