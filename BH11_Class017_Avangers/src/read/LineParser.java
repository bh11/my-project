/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package read;

import exceptions.InvalidNameAvangersException;
import hero.AbstractHero;
import hero.HeroFactory;
import store.Fleet;

/**
 *
 * @author gabri
 */
public class LineParser {
    
    private Fleet store = new Fleet();
    private static final String DELIMETER = ";";
    private static final int MIN_CHARACTERS_OF_NAME = 2;
    
    public void process(String line) throws InvalidNameAvangersException{
        String[] parameters = line.split(DELIMETER);
        checkNameRestriction(parameters[0]);
        AbstractHero hero = HeroFactory.create(parameters);
        store.add(hero);
    }
    
    public void print(){
        System.out.println(store.toString());
    }
    
    public void checkNameRestriction(String name) throws InvalidNameAvangersException{
        if (name.length()<MIN_CHARACTERS_OF_NAME) {
            throw new InvalidNameAvangersException("Invalid name: "+name);
        }
    }
    
}
