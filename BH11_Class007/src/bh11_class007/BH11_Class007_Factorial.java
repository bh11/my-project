
package bh11_class007;


public class BH11_Class007_Factorial {
    
    static int factorial(int number){
    int result = 1;
        
        for (int i = 1; i <= number; i++) {
        result *= i;    
        }
        return result;
    }

    static int recursiveFactorial(int number){
        if (number == 0 || number == 1) {
            return 1;
        }
        return number*recursiveFactorial(number-1);
        //   recursiveFactorial(5)     
            // 5* recursiveFactorial(4) 
                // 5*4* recursiveFactorial(3) 
                    // 5*4*3* recursiveFactorial(2) 
                        // 5*4*3*2* recursiveFactorial(1) 
                            // 5*4*3*2*1 
    }

    public static void main(String[] args) {

        int number = 5;
        

        System.out.println("Result: " + factorial(number));
        System.out.println(recursiveFactorial(number));
        
    }
    
}
