package bh11_class007;

public class BH11_Class007_GroupOfNumbers {

    static void writeNumberInFormat(int n) {
        int[] arr = new int[100];
        int index = 0;
        if (n == 0) {
            index = 1;
        }
        while (n > 0) {
//            System.out.print(n%1000+" ");
            arr[index] = n % 1000;
            index++;
            n = n / 1000;
        }
        for (int i = index - 1; i >= 0; i--) {
            String str = arr[i] + " ";
            if (arr[i] <= 99 && i != index - 1) {
                str = "0" + str;
            }
            if (arr[i] <= 9 && i != index - 1) {
                str = "0" + str;
            }
            System.out.print(str);
        }
        System.out.println();
    }

    static void recursiveWriteNumberInFormat(int n) {
        if (n >= 1000) {
            recursiveWriteNumberInFormat(n / 1000);
        }
        String str = n % 1000 + " ";
        if (n > 1000) {
            str = formatInteger(n % 1000) + " ";
        }
        System.out.print(str);
    }

    static String formatInteger(int i) {
        String str = "" + i;
        if (i < 99) {
            str = "0" + str;
        }
        if (i <= 9) {
            str = "0" + str;
        }
        return str;

    }

    static String harmasCsop(String szam) {
        String result = "";
        String space = " ";
        if (szam.length() == 0) {
            return result;
        }
        result += (szam.length() % 3 == 1 ? szam.substring(0, 1)+ space + harmasCsop(szam.substring(1)) : szam.substring(0, 1) + harmasCsop(szam.substring(1)));
        return result;
    }

    public static void main(String[] args) {
        /*
        Három számjegyenkénti felosztás
        Írj függvényt, amely a paraméterként kapott pozitív egész számot három számjegyenként csoportosított
        formában írja ki. Pl.: 16 077 216. Használj rekurziót!
         */
        writeNumberInFormat(16077216);
        recursiveWriteNumberInFormat(16077216);
        System.out.println();
        System.out.println(harmasCsop(""+16077216));
    }

}
