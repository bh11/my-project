package bh11_class007;

public class BH11_Class007_Szamrendszer {

    static String convertToScale(int number, int base) {
        String strTemp = "";

        while (number > 0) {
            strTemp = strTemp + (number % base);
            number = number / base;
        }
        String str = "";

        for (int i = strTemp.length() - 1; i >= 0; i--) {
            str = str + strTemp.charAt(i);
        }
        return str;
    }
    
    static void recursiveConvertToScale(int number, int base){
        if (number>0) {
            recursiveConvertToScale(number/base, base);
            System.out.println(number%base);
        }
    }

    public static void main(String[] args) {

//        System.out.println(convertToScale(2,2));
//        System.out.println(convertToScale(9,2));
//        System.out.println(convertToScale(16,2));
//        
//        System.out.println(convertToScale(2,8));
//        System.out.println(convertToScale(9,8));
//        System.out.println(convertToScale(16,8));
        
        
        convertToScale(2,8);
        convertToScale(9,8); 
        convertToScale(16,8); 

    }

}
