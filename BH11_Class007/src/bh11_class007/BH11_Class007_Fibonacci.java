
package bh11_class007;



public class BH11_Class007_Fibonacci {
    
    static int fibonacci (int number){
        if (number ==0) {
            return 0;
        }
        
        if (number == 1 || number == 2) {
            return 1;
        }
        number = 0;
        int temp1 = 0;
        int temp2 = 1;
        int sum = 0;
        
        for (int i = 2; i < number; i++) {
            sum = temp1+temp2;
            temp1 = temp2;
            temp2 = sum;
        }
        return sum;
    }
    
    static int recursiveFibo(int number) {
       if (number == 0) {
           return 0;
       }
       if(number == 1 || number == 2) {
           return 1;
       }
       return recursiveFibo(number-1) + recursiveFibo(number - 2);
   }
    
    public static void main(String[] args) {
        
  int fibo = 5;
        System.out.println(recursiveFibo(fibo));
    }
    
}
