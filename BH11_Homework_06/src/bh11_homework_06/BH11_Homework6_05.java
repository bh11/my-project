package bh11_homework_06;

import java.util.Scanner;

public class BH11_Homework6_05 {

    static Scanner scanner = new Scanner(System.in);

    static double area(double a, double b) {
        a = isPositiveNumber(a);
        b = isPositiveNumber(b);
        double area = a * b;
        return area;
    }

    static double isPositiveNumber(double number) {
        do {
            if (scanner.hasNextDouble()) {
                number = scanner.nextDouble();
                if (number > 0) {
                    return number;
                }
            } else {
                scanner.next();
            }
        } while (true);

    }

    public static void main(String[] args) {

        /*
        Készítsünk olyan függvényt, amely egy téglalap két oldalának ismeretében 
        kiszámítja a téglalap területét! Az eredmény legyen a visszatérési érték. 
        Ehhez írjunk programot, amiben ki is próbáljuk a függvényt.
         */
        double num1 = 0;
        double num2 = 0;
        System.out.println("Adj meg két pozitív számot!");
        System.out.println(area(num1, num2));
    }
}
