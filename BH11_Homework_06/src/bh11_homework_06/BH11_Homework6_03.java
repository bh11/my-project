package bh11_homework_06;

public class BH11_Homework6_03 {
    
    static void printArray(int[]array){
    System.out.print("A tömb elemei:");

        for (int i = 0; i < array.length; i++) {
            System.out.print(" " + array[i]);
        }
        System.out.println();
    }
    
    static void generateRandomElements(int[]array){
    for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100);
        }
    }

    static void backward(int[] array) {
        int[] arr2 = new int[(array.length / 2)];
        int counter = 0;
        for (int i = array.length - 2; i >= 0; i = i - 2) {
            arr2[counter] = array[i];
            counter++;
        }
        System.out.print("A tömb elemei visszafelé kettessével:");
        for (int j = 0; j < arr2.length; j++) {
            System.out.print(" " + arr2[j]);
        }
        System.out.println();
    }

    public static void main(String[] args) {

        /*
        Írj függvényt, ami egy tömböt átvesz paraméterként és hátulról 
        indulva kiírja minden második elemét! Ügyelj arra, hogy nehogy túl/alulindexeld a tömböt! 
         */
        int[] array = new int[20];

        generateRandomElements(array);
        printArray(array);
        backward(array);
    }

}
