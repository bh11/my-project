package bh11_homework_06;

import java.util.Scanner;

public class BH11_Homework6_01 {

    static Scanner scanner = new Scanner(System.in);

    static int isPositiveInteger(int number) {
        do {
            if (scanner.hasNextInt()) {
                number = scanner.nextInt();
                if (number > 0) {
                    return number;
                }
            } else {
                scanner.next();
            }
        } while (true);

    }

    static boolean isPrime(int number) {
        int counter = 0;
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                counter++;
            }
        }
        if (counter > 2 || number == 1) {
            return false;
        } else {
            return true;
        }
    }

    static int numberOfPrime(int counter) {
        counter = isPositiveInteger(counter);
        int num = 0;
        int prime = 0;
        for (int i = 1; num < counter; i++) {
            if (isPrime(i)) {
                num++;
            }
            prime = i;
        }
        return prime;
    }

    public static void main(String[] args) {

        /**
         * Írj függvényt, amely a paramétereként átvett pozitív, egész számról
         * eldönti, hogy az prím-e! Logikai visszatérése legyen, amely akkor
         * IGAZ, ha a paraméterként átvett szám prím, különben HAMIS. Írj
         * programot, amely a felhasználótól bekér egy számot, és a fenti
         * függvény segítségével kiírja a képernyőre az annyiadik prímszámot
         */
        int number = 0;
        System.out.println("Adj meg egy pozitív egész számot!");
        System.out.println(isPrime(isPositiveInteger(number)));
        System.out.println("Adj meg egy pozitív egész számot, megadom, hogy mi az annyiadik prímszám!");
        System.out.println(numberOfPrime(number));
    }

}
