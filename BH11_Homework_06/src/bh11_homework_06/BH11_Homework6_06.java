package bh11_homework_06;

public class BH11_Homework6_06 {

    static void divide3ButNot5(int[] array) {
        int counter = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] % 3 == 0 && array[i] % 5 != 0) {
                counter++;
            }
        }
        System.out.printf("A tömbben %ddb 3-mal igen, de 5-tel nem osztható szám van \n", counter);
    }

    public static void main(String[] args) {

        /*
        Készítsünk olyan függvényt, amely meghatározza egy tömbben 
        hány 3-al osztható, de 5-el nem osztható szám van!
         */
        int[] array = new int[20];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100);
        }

        System.out.print("A tömb elemei:");
        for (int i = 0; i < array.length; i++) {
            System.out.print(" " + array[i]);
        }
        System.out.println();

        divide3ButNot5(array);
    }

}
