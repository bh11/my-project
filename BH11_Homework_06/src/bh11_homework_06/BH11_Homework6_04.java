package bh11_homework_06;

import java.util.Scanner;

public class BH11_Homework6_04 {

    static Scanner scanner = new Scanner(System.in);

    static char upperCase(char character) {
        character = scanner.nextLine().charAt(0);
        character = Character.toUpperCase(character);
        return character;
    }

    public static void main(String[] args) {

        /*
        Írj egy függvényt, ami a paraméterként átvett kis betűnek megfelelő nagybetűt adja vissza!
         */
        char c = '\0';
        System.out.println(upperCase(c));

    }
}
