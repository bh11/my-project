package bh11_homework_06;

import java.util.Scanner;


public class BH11_Homework6_02 {
    
    static Scanner scanner = new Scanner(System.in);
    
    static int absolute(int num1, int num2){
        num1 = isInteger(num1);
        num2 = isInteger(num2);
        if (Math.abs(num1)>Math.abs(num2)) {
            return num1;
        }else{
        return num2;
        }
    }
    
    static int isInteger(int number){
            do {
            if (scanner.hasNextInt()) {
                number = scanner.nextInt();
                    return number;
            } else {
                scanner.next();
            }
        } while (true);

    }
    

    public static void main(String[] args) {

        /*
        Írj függvényt, amely paraméterként átvesz két egész számot és 
        visszaadja azt, amelyiknek az abszolút értéke nagyobb
         */
    int num1 = 0;
    int num2 = 0;
        System.out.println("Adj meg két egész számot!");
        System.out.println(absolute(num1, num2));
    }

}
