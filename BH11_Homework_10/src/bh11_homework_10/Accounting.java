package bh11_homework_10;

public class Accounting {

    private String accountingID;
    private String country;
    private final static int MAX_NUMBER_OF_BILLS = 10;
    private Bill[] bills = new Bill[MAX_NUMBER_OF_BILLS];
    private Bill bill;
    int counter = 0;

    public Accounting(String accountingID, String country) {
        this.country = country;
        this.accountingID = accountingID;

    }

    public void addBill(Bill bill) {
    this.bill = bill;
        if (counter<MAX_NUMBER_OF_BILLS) {
            bills[counter]=bill;
            counter++;
        }
    }
    
    
        public void printBill(){
        for (int i = 0; i < bills.length; i++) {
            if (bills[i]!= null) {
              System.out.println(bills[i].getSerialNumber()+" "+bills[i].getMoney());  
            }  
        }
    }

     public void increaseOrDecreaseTheBills(double number){
        for (int i = 0; i < bills.length; i++) {
            if (bills[i]!= null) {
              bills[i].setmoney((byte) (bills[i].getMoney()*number));  
            }  
        }
    }



    public String getCountry() {
        return country;
    }

    public String getAccountingID() {
        return accountingID;
    }
    
//    public void setCountry(String country) {
//        this.country = country;
//    }
//
//    public void setAccountingID(String accountingID) {
//        this.accountingID = accountingID;
//    }
//
}
