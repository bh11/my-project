package bh11_homework_10;

public class Bill {

    private int money;
    private String serialNumber;
    private final static int DEFAULT_SUM = 1000;

   public Bill(String serialNumber) {
       this(serialNumber, DEFAULT_SUM);
    }

   public Bill(String serialNumber, int money) {
        this.serialNumber = serialNumber;
        this.money = money;
    }

    public void setmoney(int money) {
        this.money = money;
    }

    public int getMoney() {
        return money;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

//    public void setSerialNumber(String serialNumber) {
//        this.serialNumber = serialNumber;
//    }
//
}
