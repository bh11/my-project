package bh11_homework_10;

public class Employee {

    private String name;
    private String systemID;
    private Accounting[] bookEntry;
    private Accounting accounting;
    private int counter;
    private final static double INCREASE_MONEY = 1.1;
    private final static double DECREASE_MONEY = 0.9;

    Employee() {
        bookEntry = new Accounting[2];
    }

    Employee(String name, String systemID) {
        this.name = name;
        this.systemID = systemID;
    }

    public void printBookEntryAndBill() {
        for (int i = 0; i < bookEntry.length; i++) {
            System.out.println(bookEntry[i].getAccountingID() + " " + bookEntry[i].getCountry());
            bookEntry[i].printBill();
            System.out.println();
        }
    }

    public void increaseBills() {
        accounting.increaseOrDecreaseTheBills(INCREASE_MONEY);
    }

    public void decreaseBills() {
        accounting.increaseOrDecreaseTheBills(DECREASE_MONEY);
    }

    public void addToBookEntry(Accounting accounting, Accounting accounting2) {
        if (counter < 2) {
            this.accounting = accounting;
            bookEntry[0] = accounting;
            this.accounting = accounting2;
            bookEntry[1] = accounting2;
        }

    }

//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public void setSystemID(String systemID) {
//        this.systemID = systemID;
//    }
}
