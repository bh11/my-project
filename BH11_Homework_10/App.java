package bh11_homework_10;

public class App {

    public static void main(String[] args) {

        /**
         * Egy cégnél vannak Dolgozók (név: String, rendszer id: String,
         * könyvelési egységek: Array), minden Dolgozó képes Könyveléseket (id:
         * String, ország: String, számlák: Array) kezelni. Minden Könyvelési
         * egységhez tartozhatnak Számlák (összeg: int, sorszám: String). A
         * dolgozókat létre tudjuk hozni Könyvelési egységek megadásával. Minden
         * dolgozó pontosan kettő Könyvelési egységet kezel. A Könyvelési
         * egységeket id és ország alapján tudjuk létrehozni. A könyvelési
         * egységek egy tömbben maximum 10 darab számlát tárolnak. A Számla
         * kötelező eleme a sorszám. Az összeg is megadható létrehozáskor, de ha
         * nem adjuk meg, akkor 1000 USD értékű a számla. A Dolgozó képes az
         * összes hozzátartozó számla 10%-os növelésére vagy csökkentésére (ez
         * legyen 2 külön metódus). A dolgozó képes kilistázni a hozzátartozó
         * számlákat (sorszám - összeg). Példányosítsuk a létrehozott
         * osztályokat, írjuk ki a számlákat (sorszám - összeg). Majd növeljük
         * meg minden számla értékét. Írjuk ki az új értékeket.
         */
        

        // Nem sikerült a növelés és a csökkentés
        // Hogyan kell konstruktoron belül konstruktorra hivatkozni úgy, hogy van kivétel?


        Employee emp1 = new Employee();
        Employee emp2 = new Employee();

        Accounting account1 = new Accounting("12231", "Germany");
        Accounting account2 = new Accounting("43242", "Hungary");
        Accounting account3 = new Accounting("56463", "Italy");
        Accounting account4 = new Accounting("12523", "Spain");

        Bill invoice1 = new Bill("122");
        Bill invoice2 = new Bill("221", 2000);
        Bill invoice3 = new Bill("332", 1500);
        Bill invoice4 = new Bill("544");
        Bill invoice5 = new Bill("867");
        Bill invoice6 = new Bill("322");
        Bill invoice7 = new Bill("123", 6000);
        Bill invoice8 = new Bill("021");
        Bill invoice9 = new Bill("532", 7800);
        Bill invoice10 = new Bill("031", 9250);

        account1.addBill(invoice1);
        account1.addBill(invoice2);
        account1.addBill(invoice3);
        account1.addBill(invoice4);
        account1.addBill(invoice5);
        account2.addBill(invoice6);
        account2.addBill(invoice7);
        account2.addBill(invoice8);
        account2.addBill(invoice9);
        account2.addBill(invoice10);

        emp1.addToBookEntry(account1, account2);
        emp2.addToBookEntry(account3, account4);

        emp1.printBookEntryAndBill();
        emp1.increaseBills();
        emp1.printBookEntryAndBill();
    }

}
