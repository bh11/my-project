package bh11_class014_string;

import java.util.HashMap;
import java.util.Map;

public class BH11_Class014_String {

    public static void main(String[] args) {

        String str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed "
                + "do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim "
                + "ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex "
                + "ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit "
                + "esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non "
                + "proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

        /*
      A hashMap egy olyan adatstruktúra, ami kulcsot egy értékhez tud párosítani 
        Map<key, value> map = new HashMap<>();
        map.put(key,value);
                
         */
        Map<Character, Integer> map = new HashMap<>();

        for (int i = 0; i < str.length(); i++) {
            Character ch = str.charAt(i);
            Integer count = map.containsKey(ch) ? map.get(ch) + 1 : 1;
            map.put(str.charAt(i), count);
        }
    }

}
