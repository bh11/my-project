
package bh11_homework_11;


public class BH11_Homework_11 {


    public static void main(String[] args) {
/*

 A lényeg mindhárom utódosztályban definiáld felül a say metódust, és eza metódus írja ki, hogy az adott állat milyen hangot ad ki. Mindenhol definiáld felül a toString-et!
 Készíts egy 10 elemű tömböt, töltsd fel az állatokkal, majd egy for ciklusban hívd meg a say metódust.
        */

    
    Animal[] animals ={new Cat(), new Dog(), new Snake(), new Cat(), new Cat(), new Dog(), new Snake(), new Dog(), new Cat(), new Snake()};
    
        for (int i = 0; i < animals.length; i++) {
    animals[i].say();
        }
    }
    
}
