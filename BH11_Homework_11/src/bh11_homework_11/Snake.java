package bh11_homework_11;

public class Snake extends Animal {

    private boolean reptile;

    @Override
    public void say() {
        System.out.println("Ssssssssssss");
    }

    @Override
    public String toString() {
        return "Snake{" + "reptile=" + reptile + '}';
    }

}
