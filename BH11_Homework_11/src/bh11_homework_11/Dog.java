package bh11_homework_11;

public class Dog extends Animal {

    private String color;

    @Override
    public void say() {
        System.out.println("Wuf-wuf");
    }

    @Override
    public String toString() {
        return "Dog{" + "color=" + color + '}';
    }

}
