package bh11_homework_11;

public class Cat extends Animal {

    private boolean domestic;

    @Override
    public void say() {
        System.out.println("Meeeooow");
    }

    @Override
    public String toString() {
        return "Cat{" + "domestic=" + domestic + '}';
    }

}
