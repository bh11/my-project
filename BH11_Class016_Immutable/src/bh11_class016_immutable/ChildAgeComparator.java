
package bh11_class016_immutable;

import java.util.Comparator;


public class ChildAgeComparator implements Comparator<Child>{

    @Override
    public int compare(Child c1, Child c2) {
        return c1.getAge()-c2.getAge();
    }
    
}
