package bh11_class016_immutable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {

        List<Child> children = new ArrayList<>();
        children.add(new Child("Juliska", 9));
        children.add(new Child("Jancsi", 12));

        ImmutableParent parent = new ImmutableParent("Boszorkány", 90, children);

        // parent.getChildren().add(new Child("Béla", 15)); //--> UnsupportedOperationException
        ImmutableParent parent1 = new ImmutableParent("Anna", 40, children);
        ImmutableParent parent2 = new ImmutableParent("Béla", 45, children);
        ImmutableParent parent3 = new ImmutableParent("Sanyi", 90, children);
        ImmutableParent parent4 = new ImmutableParent("Kata", 30, children);

        Set<ImmutableParent> parents = new TreeSet<>();
        parents.add(parent1);
        parents.add(parent2);
        parents.add(parent3);
        parents.add(parent4);

        for (ImmutableParent p : parents) {
            System.out.println(p);
        }

        Child c1 = new Child("Peti", 9);
        Child c2 = new Child("Jani", 9);
        Child c3 = new Child("Julcsi", 12);
        Child c4 = new Child("Julcsi", 13);

        Set<Child> childrenTreeSetByAge = new TreeSet<>(new ChildAgeComparator());
        childrenTreeSetByAge.add(c1);
        childrenTreeSetByAge.add(c2);
        childrenTreeSetByAge.add(c3);
        childrenTreeSetByAge.add(c4);

        for (Child child : childrenTreeSetByAge) {
            System.out.println(child);
        }

        Set<Child> childrenTreeSetByName = new TreeSet<>(new ChildNameComparator());
        childrenTreeSetByName.add(c1);
        childrenTreeSetByName.add(c2);
        childrenTreeSetByName.add(c3);
        childrenTreeSetByName.add(c4);

        System.out.println();
        for (Child child : childrenTreeSetByName) {
            System.out.println(child);
        }

        //Keressük meg a legidősebb gyereket
        Set<Child> kids = new HashSet<>();
        kids.add(c1);
        kids.add(c2);
        kids.add(c3);
        kids.add(c4);
        
        System.out.println();
        Child oldest = Collections.max(kids, new ChildAgeComparator());
        System.out.println(oldest);

        //Töröljük ki a legidősebb gyereket
        Iterator<Child> ic =kids.iterator();
        while(ic.hasNext()){
        Child child = ic.next();
            if (child.equals(oldest)) {
                ic.remove();
            }
        }
        System.out.println("*******************************************");
        for (Child kid : kids) {
            System.out.println(kid);
        }
    }
}
