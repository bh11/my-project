package bh11_homework_07;

import java.util.Scanner;

public class BH11_Homework7_03 {

    static int generateInteger() {
        Scanner scanner = new Scanner(System.in);
        do {
            if (scanner.hasNextInt()) {
                int number = scanner.nextInt();
                return number;
            } else {
                scanner.next();
            }
        } while (true);

    }

    static int sumOfDigits(int number) {
        if (number == 0) {
            return 0;
        }
        return (number % 10 + sumOfDigits(number / 10));
    }

    public static void main(String[] args) {

        /*
         - adott szám számjegyeinek összege rekurzív módon
         */
//    int number = 11111;
//    int sum = 0;
//        while(number>0){
//        sum += number%10;
//        number /= 10;
//        }
//        System.out.println(sum);
        System.out.println(sumOfDigits(generateInteger()));
    }
}
