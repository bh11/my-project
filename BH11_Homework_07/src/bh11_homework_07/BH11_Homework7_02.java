
package bh11_homework_07;

public class BH11_Homework7_02 {
    
    static void printArray(int[]array){

        for (int i = 0; i < array.length; i++) {
            System.out.print(" " + array[i]);
        }
        System.out.println();
    }
    
    static int[] randomArray(int[] array){
    for (int i = 0; i < array.length; i++) {
            array[i]= (int)(Math.random()*100);
        }
    return array;
    }
    public static void main(String[] args) {
        
        /*
        leggyakoribb szám: 0-100 közötti véletlen számokkal töltsünk fel 
        egy 300 elemű tömböt, majd keressük meg h melyik szám került bele a leggyakrabban
        */
    int[] array = new int[300];
    
        randomArray(array);
        printArray(array);
        
        int[] temp = new int[100];
        
        for (int i = 0; i < array.length; i++) {
            temp[array[i]]++;
            } 

        printArray(temp);

        int value = temp[0];
        int index = 0;
        
        for (int i = 0; i < temp.length; i++) {
            if (value<temp[i]) {
                value = temp[i];
                index = i;
            }
        }
        System.out.printf("A tömbben a leggyakoribb szám a(z) %d, %d alkalommal fordul elő.\n", index, value);
    }
    
}
