
package bh11_homework_07;


public class BH11_Homework7_04 {
    
    static int[][] generateRandomArrayElements(int[][] array){
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j]= (int)(Math.random()*10);
            }
        }
        return array;
    }
    
     static void print2DArray(int[][]array){

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(" "+array[i][j]);
            }
            System.out.println();
        }
    }
        static void printArray(int[]array){

        for (int i = 0; i < array.length; i++) {
            System.out.print(" " + array[i]);
        }
        System.out.println();
    }
    static int[] minimumNumber(int[][]array){
    int min = array[0][0];
    int row = 0;
    int col = 0;
    
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (min>array[i][j]) {
                    min=array[i][j];
                    row = i;
                    col = j;
                }
            }
        }
        int[] result = {row, col, min};
        return result;
    }
    public static void main(String[] args) {
        /*
         - írjunk metódust, ami egy n*m-es kétdimenziós tömböt feltölt véletlenszerű elemekkel 
        valamilyen határok között, majd írjunk egy másik metódust, ami egy n*m-es kétdimenziós 
        tömb legkisebb elemével és annak helyével tér vissza. A visszatérés egy tömb legyen, 
        az alábbi formában [sor, oszlop, érték]
        */
    
    int[][] array = new int[5][5];
        generateRandomArrayElements(array);
        print2DArray(array);
        printArray(minimumNumber(array));

    }

    
}
