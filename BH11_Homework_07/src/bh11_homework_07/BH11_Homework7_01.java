package bh11_homework_07;

import java.util.Scanner;

public class BH11_Homework7_01 {

    static Scanner scanner = new Scanner(System.in);

    static double celsiusToFarenheit(int celsius) {
        double farenheit;
        farenheit = (celsius + 273.15) * 9 / 5 - 459.67;
        return farenheit;
    }

    static int generateInteger() {
        do {
            if (scanner.hasNextInt()) {
                int number = scanner.nextInt();
                return number;
            } else {
                scanner.next();
            }
        } while (true);

    }

    public static void main(String[] args) {

        //Celsius -> Fahrenheit átváltó függvény

        System.out.println(celsiusToFarenheit(generateInteger()));
    }

}
