package stone;

public abstract class AbstractStone {

    private final String color;
    private final int power;

    private static final int MIN_POWER = 2;
    private static final int MAX_POWER = 10;
    


    public AbstractStone(String color) {
        this.color = color;
        this.power = generatePower();
    }

    public String getColor() {
        return color;
    }

    public int getPower() {
        return power;
    }

    @Override
    public String toString() {
        return "AbstractStone{" + "color=" + color + ", power=" + power + '}';
    }

    private int generatePower() {
        return (int) (Math.random() * (MAX_POWER - MIN_POWER) + MIN_POWER);
    }
}
