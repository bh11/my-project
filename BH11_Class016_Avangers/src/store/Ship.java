package store;

import hero.AbstractHero;
import java.util.ArrayList;
import java.util.List;

public class Ship {

    public static final int MAX_SIZE = 4;
    private final List<AbstractHero> heroes = new ArrayList<>();

    public void add(AbstractHero hero) {
        if (isEmptySeat()) {
            heroes.add(hero);
        }
    }

    public boolean isEmptySeat() {
        return heroes.size() < MAX_SIZE;
    }

    @Override
    public String toString() {
        return "Ship{" + "heroes=" + heroes + '}';
    }
}
