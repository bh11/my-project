/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package read;

import hero.AbstractHero;
import hero.HeroFactory;
import store.Fleet;

/**
 *
 * @author gabri
 */
public class LineParser {
    
    private Fleet store = new Fleet();
    private static final String DELIMETER = ";";
    
    public void process(String line){
        AbstractHero hero = HeroFactory.create(line.split(DELIMETER));
        store.add(hero);
    }
    
    public void print(){
        System.out.println(store.toString());
    }
    
}
