package hero;

import stone.Time;

public class HeroFactory {

    public static final String EARTH = "0";
    public static final String NOT_EARTH = "1";

    public static AbstractHero create(String[] parameters) {
        String name = parameters[0];
        int power = Integer.parseInt(parameters[1]);
        Time AbstractStone = new Time();

        if (EARTH.equals(parameters[3])) {
            return new BornOnEarth(name, power, AbstractStone, new IdentityCard());
        } else {
            return new NotBornOnEarth(name, power, AbstractStone);
        }
    }
}
