
package hero;

import ability.Firing;
import ability.Swimming;
import stone.AbstractStone;


public class NotBornOnEarth extends AbstractHero implements Swimming, Firing{
    
    public NotBornOnEarth(String name, int power, AbstractStone stone) {
        super(name, power, stone);
    }
    
    @Override
    public void fire(){
        System.out.println("I can throw fireballs.");
    }
    
    @Override
    public void swim(){
        System.out.println("I can swim.");
    }
    
    
}
