
package bh_11_class003;

import java.util.Scanner;

public class BH_11_Class003_Ciklusok {
    public static void main(String[] args) {
     
//       int i = 1;
//       
//       while (i<=10) {
//           System.out.println(i+ ". Zsuga T. Gabriella");
//           i++;
//       }
        
       /*
       Kérjünk be a felhasználótól 5 számot, és írjuk ki az összegét
       */
       
        Scanner scanner = new Scanner(System.in);
//        System.out.println("Írj be 5 számot!");
//        int j = 0;
//        int sum = 0;
//       
//        while (j<5) {
//        int number = scanner.nextInt();
//        sum+=number;
//        j++;
//                
//        }
//        System.out.println("Összeg: "+sum);
   
    
        do {
            
            System.out.println("Legalább egyszer lefut");
            
        } while (false);
     
        /*
        Addig kérjünk be számot a felhasználótól, amíg 0-t nem ad!
        */
        int a;
        int stopNumber = 0;
        do {
            a = scanner.nextInt();
            if (a != stopNumber) {
                System.out.println("Out: "+a);
            }
        } while (a != stopNumber);
        
    
    }
    
}
