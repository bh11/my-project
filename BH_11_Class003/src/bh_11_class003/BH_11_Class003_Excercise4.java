/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh_11_class003;

import java.util.Scanner;

/**
 *
 * @author Gabriella
 */
public class BH_11_Class003_Excercise4 {

    public static void main(String[] args) {

        /*
         A felhasználó megad egy pozitív egész számot. 
         Adjuk meg a pozitív osztóit. 
         */
        Scanner scanner = new Scanner(System.in);
        int number = 0;
        int a = -1;
        do {
            System.out.println("Adjon meg egy pozitív egész számot!");
            if (scanner.hasNextInt()) {
                number = scanner.nextInt();
            } else {
            scanner.nextLine();
            }
        } while (number <0);

      int divisor = 1;
       while (divisor <= (number)) {
            if (number % divisor == 0) {
                System.out.println(divisor);
            }
            divisor++;
        }
       int countOfDivider = 0;
       int j = 1;
       while (j<=number){
           if (number % j == 0) {
               countOfDivider++;
           }
           j++;
       }
       
       if (countOfDivider == 2){
           System.out.println("Prím");
       }else{
           System.out.println("Nem prím");
       }
    }

}
