/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh_11_class003;

import java.util.Scanner;

/**
 *
 * @author Gabriella
 */
public class BH_11_Class003_Excercise3 {

    public static void main(String[] args) {

        //A felhasználó megad 1-től 5-ig egy számot, és írjuk ki római számmal
        Scanner scanner = new Scanner(System.in);
        System.out.println("Adjon meg egy számot 1-től 5-ig");
        int number = -1;
        boolean asking = true;

        do {
            if (scanner.hasNextInt()) {
                number = scanner.nextInt();
                if (number >= 1 && number <= 5) {
                    asking = false;
                }
            } else {
            scanner.next();
            }
        } while (asking);

        switch (number) {
            case 1:
                System.out.println("I.");
                break;
            case 2:
                System.out.println("II.");
                break;
            case 3:
                System.out.println("III.");
                break;
            case 4:
                System.out.println("IV.");
                break;
            case 5:
                System.out.println("V.");

        }
        
        char a = 'A';
        
        while (a<='Z'){
            System.out.println(a);
            a++;
               
        }

    }

}
