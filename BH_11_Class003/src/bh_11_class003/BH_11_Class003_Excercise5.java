/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh_11_class003;

import java.util.Scanner;

/**
 *
 * @author Gabriella
 */
public class BH_11_Class003_Excercise5 {

    public static void main(String[] args) {

        /*
         Írjuk ki 20-tól 945-ig a 3-mal osztható, de 7-tel nem osztható számokat! 
         */
        int i = 20;
        while (i <= 945) {
            if (i % 3 == 0 && i % 7 != 0) {
                System.out.print(i + ", ");

            }
            i++;
        }
        System.out.println();

        /*
         A felhasználó megad 2 számot. Írjuk ki a kisebbtől a nagyobbig a 7-re
         végződő számokat! 10-zel osztva 7 lesz a maradéka
         */
        Scanner scanner = new Scanner(System.in);
        System.out.println("Adj meg 2 egész számot!");
        int num1 = 0;
        int num2 = 0;
        boolean isValidinput = false;

        do {
            System.out.println("Első szám:");
            if (scanner.hasNextInt()) {
                num1 = scanner.nextInt();
                isValidinput = true;
            } else {
                scanner.nextLine();
            }
        } while (!isValidinput);

        isValidinput = false;

        do {
            System.out.println("Második szám:");
            if (scanner.hasNextInt()) {
                num2 = scanner.nextInt();
                isValidinput = true;
            } else {
                scanner.nextLine();
            }
        } while (!isValidinput);

        int min = Math.min(num1, num2);
        int max = Math.max(num1, num2);
        
        while (min <= max){
            if (min % 10 == 7) {
                System.out.println(min);
            }
            min++;
        }
        
    }

}
