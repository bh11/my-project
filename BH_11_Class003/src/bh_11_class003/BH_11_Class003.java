package bh_11_class003;

import java.util.Scanner;

public class BH_11_Class003 {

    public static void main(String[] args) {
        //ismétlés

        long a = 100000000L;
        byte b = (byte) 128;
        float f = (float) 5.666;
        float f2 = 5.666f;
        System.out.println(a);
        System.out.println(b);

        //Switch
        //Kérjünk be 1-től 5-ig egy számot és írjuk ki az osztályzatot
        Scanner scanner = new Scanner(System.in);

        System.out.println("Írj be egy számot 1-től 5-ig!");
        int number = scanner.nextInt();

        switch (number) {
            case 1:
                System.out.println("Elégtelen");
                break;
            case 2:
                System.out.println("Elégséges");
                break;
            case 3:
                System.out.println("Közepes");
                break;
            case 4:
                System.out.println("Jó");
                break;
            case 5:
                System.out.println("Jeles");
                break;
            default:
                System.out.println("Hibás input!");
    }
                if (number == 1) {
                    System.out.println("Elégtelen");
                } else if (number == 2) {
                    System.out.println("Elégséges");
                } else if (number == 3) {
                    System.out.println("Közepes");
                } else if (number == 4) {
                    System.out.println("Jó");
                } else if (number == 5) {
                    System.out.println("Jeles");
                } else {
                    System.out.println("Hibás input!");
                }
        }

    

}
