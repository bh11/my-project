/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh_11_class003;

import java.util.Scanner;

/**
 *
 * @author Gabriella
 */
public class BH_11_Class003_Exercise2 {
    
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Írj be egy számot 1-től 5-ig");
        int number = scanner.nextInt();
        
        switch (number){
            case 1:
            case 2:
            case 3: System.out.println("Nem felelt meg!"); break;
            case 4:
            case 5: System.out.println("Megfelelt!"); break;
            default: System.out.println("Hibás input!");
        }
    }
    
}
