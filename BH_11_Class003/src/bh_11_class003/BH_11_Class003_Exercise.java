/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh_11_class003;

import java.util.Scanner;

/**
 *
 * @author Gabriella
 */
public class BH_11_Class003_Exercise {

    public static void main(String[] args) {
        /*
         Kérjünk be a felhasználótól egy karaktert, és mondjuk meg róla, hogy 
         az kisbetű, nagybetű vagy szám.
         */

        Scanner scanner = new Scanner(System.in);

        System.out.println("Kérlek írj be egy karaktert!");
        char a = scanner.next().charAt(0);

        if (a >= 48 && a <= 57) { // --> (a>= '0' && a<= '9')
            System.out.println("Ez a karakter egy szám");
        } else if (a >= 65 && a <= 90) { //--> (a>= 'A' && a<='Z')
            System.out.println("Ez a karakter nagybetű");
        } else if (a >= 97 && a <= 172) { //--> (a>='a' && a<='A')
            System.out.println("Ez a karakter kisbetű");
        } else {
            System.out.println("Hibás input!");
        }

        /*
         if/switch használatával rendszerezd a dombozati formákat!
         */
        System.out.println("Ad meg egy pozitív egész számot!");
        int number = scanner.nextInt();
        if (number < 0) {
            System.out.println("Hibás input!");
        } else if (number <= 200) {
            System.out.println("Alföld");
        } else if (number <= 500) {
            System.out.println("Dombság");
        } else if (number <= 1500) {
            System.out.println("Középhegység");
        } else {
            System.out.println("Hegység");
        }

        /*
         A felhasználó adjon meg 3 számot, és döntsük el, hogy lehet-e belőle 
         egyenlő szárú háromszöget szerkeszteni.
         */
        System.out.println("Ad meg 3 pozitív egész számot!");
        int n1 = scanner.nextInt();
        int n2 = scanner.nextInt();
        int n3 = scanner.nextInt();
        if (n1 + n2 > n3 && n1 + n3 > n2 && n2 + n3 > n1) {
            if (n1 == n2 || n1 == n3 || n2 == n3) {
                System.out.println("A háromszög egyenlő szárú háromszög");
            } else {
                System.out.println("A háromszög nem egyenlő szárú háromszög");
            }
        } else {
            System.out.println("Ezekből a számokból nem lehet háromszöget szerkeszteni");
        }

    }

}
