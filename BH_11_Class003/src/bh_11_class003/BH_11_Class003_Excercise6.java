/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bh_11_class003;

/**
 *
 * @author Gabriella
 */
public class BH_11_Class003_Excercise6 {

    public static void main(String[] args) {
        //írjuk ki a 10-es szórzótáblát!

        int a = 1;
        int b = 10;

        while (a <= 10) {
            System.out.printf("10 * %d = %d\n", a, a * b);
            a++;
        }
        System.out.println();

        // teljes szorzótábla
        int m = 1;
        while (m <= 10) {
            int n = 1;
            while (n <= 10) {
                System.out.printf("%d * %d = %d\n", m, n, m * n);
                n++;
            }
            m++;
            System.out.println();
        }

//Szorzótábla for ciklussal
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 10; j++) {
                System.out.printf("%d * %d = %d\n", i, j, i * j);
            }
            System.out.println();
        }

    }

}
